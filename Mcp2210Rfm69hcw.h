/******************************************************************************/
/* (C) PulsingCore Software Ltd 2020                                          */
/******************************************************************************/
/*                                                                            */
/* Mcp2210Rfm69Hcw.h                                                          */
/* 23.07.2020                                                                 */
/* Paul O'Brien                                                               */
/*                                                                            */
/* - definitions and declarations for the MCP2210 <--> RFM69HCW interface     */
/*                                                                            */
/******************************************************************************/

#ifndef _MCP2210_RFM69HCW_H_
#define _MCP2210_RFM69HCW_H_

/******************************************************************************/

// The register address is one-byte wide
#define MCP2210_RFM69HCW_REGISTER_SETUP_ADDRESS_WIDTH  (1)
// Almost all accesses to the RFM69HCW registers are only one-byte wide
#define MCP2210_RFM69HCW_REGISTER_SETUP_MINIMUM_WIDTH  (1)
// The transmit/receive FIFO is 66-bytes deep
#define MCP2210_RFM69HCW_REGISTER_SETUP_MAXIMUM_WIDTH (66)

/******************************************************************************/

#endif

/******************************************************************************/
/* (C) PulsingCore Software Ltd 2020                                          */
/******************************************************************************/
