/******************************************************************************/
/* (C) PulsingCore Software Limited 2020                                      */
/******************************************************************************/
/*                                                                            */
/* ApvTypes.h                                                                 */
/* 11.06.20                                                                   */
/*                                                                            */
/* copied from the definitions in "APVTypes.h"                                */
/*                                                                            */
/* Ref : NMEA 0183 Specification v2.3                                         */
/*       ublox NEO-6 - Data Sheet                                             */
/*                                                                            */
/******************************************************************************/

#ifndef _APV_TYPES_H_
#define _APV_TYPES_H_

/******************************************************************************/

#include <stdbool.h>
#include <stdint.h>

/******************************************************************************/

#ifdef _APV_PARSER_WIN_INLINE_
#define _APV_INLINE_INCLUDE_  //inline
#define _APV_INLINE_FUNCTION_ //inline
#else
#define _APV_INLINE_INCLUDE_  //__inline__
#define _APV_INLINE_FUNCTION_ //inline
#endif

#define APV_TYPE_VOID             void
#define APV_TYPE_BOOLEAN          bool
#define APV_TYPE_INT8             int8_t
#define APV_TYPE_UINT8            uint8_t
#define APV_TYPE_CHAR             char
#define APV_TYPE_UCHAR            unsigned char
#define APV_TYPE_INT16            int16_t
#define APV_TYPE_UINT16           uint16_t
#define APV_TYPE_SHORT            short
#define APV_TYPE_USHORT           unsigned short
#define APV_TYPE_INT32            int32_t
#define APV_TYPE_UINT32           uint32_t
#define APV_TYPE_INT              int
#define APV_TYPE_UINT             unsigned int 
#ifndef _APV_NUMERICAL_RANGE_ARM32_
#define APV_TYPE_INT64            int64_t
#define APV_TYPE_UINT64           uint64_t
#endif
#define APV_TYPE_LONG                      long
#define APV_TYPE_ULONG            unsigned long
#define APV_TYPE_FLOAT            float
#ifndef _APV_NUMERICAL_RANGE_ARM32_
#define APV_TYPE_DOUBLE           double
#define APV_TYPE_LONG_DOUBLE      long double
#endif

// Macro expansion substitutions for numerical types
#define APV_TYPE_LONG_SUBSTITUTION         .apv_long_t
#define APV_TYPE_ULONG_SUBSTITUTION        .apv_ulong_t
#ifndef _APV_NUMERICAL_RANGE_ARM32_        
#define APV_TYPE_INT64_SUBSTITUTION        .apv_int64_t
#define APV_TYPE_UINT64_SUBSTITUTION       .apv_uint64_t
#endif                                     
#define APV_TYPE_FLOAT_SUBSTITUTION        .apv_float_t
#ifndef _APV_NUMERICAL_RANGE_ARM32_
#define APV_TYPE_DOUBLE_SUBSTITUTION       .apv_double_t
#define APV_TYPE_LONG_DOUBLE_SUBSTITUTION  .apv_long_double_t
#endif

/******************************************************************************/

typedef enum apvNumericalFormats_tTag
  {
  APV_NUMERICAL_FORMAT_VOID = 0,
  APV_NUMERICAL_FORMAT_BOOLEAN,
  APV_NUMERICAL_FORMAT_INT8,
  APV_NUMERICAL_FORMAT_UINT8,
  APV_NUMERICAL_FORMAT_CHAR,
  APV_NUMERICAL_FORMAT_UCHAR,
  APV_NUMERICAL_FORMAT_INT16,
  APV_NUMERICAL_FORMAT_UINT16,
  APV_NUMERICAL_FORMAT_SHORT,
  APV_NUMERICAL_FORMAT_USHORT,
  APV_NUMERICAL_FORMAT_INT32,
  APV_NUMERICAL_FORMAT_UINT32,
  APV_NUMERICAL_FORMAT_INT,
  APV_NUMERICAL_FORMAT_UINT,
#ifndef _APV_NUMERICAL_RANGE_ARM32_
  APV_NUMERICAL_FORMAT_INT64,
  APV_NUMERICAL_FORMAT_UINT64,
#endif
  APV_NUMERICAL_FORMAT_LONG,
  APV_NUMERICAL_FORMAT_ULONG,
  APV_NUMERICAL_FORMAT_FLOAT,
#ifndef _APV_NUMERICAL_RANGE_ARM32_
  APV_NUMERICAL_FORMAT_DOUBLE,
  APV_NUMERICAL_FORMAT_LONG_DOUBLE,
#endif
  APV_NUMERICAL_FORMATS
  } apvNumericalFormats_t;

typedef enum apvNumericalLimitTests_tTag
  {
  APV_NUMERICAL_LIMIT_TEST_NOT_EQUAL = 0,
  APV_NUMERICAL_LIMIT_TEST_EQUAL,
  APV_NUMERICAL_LIMIT_TEST_GREATER,
  APV_NUMERICAL_LIMIT_TEST_GREATER_OR_EQUAL,
  APV_NUMERICAL_LIMIT_TEST_LESSER,
  APV_NUMERICAL_LIMIT_TEST_LESSER_OR_EQUAL,
  APV_NUMERICAL_LIMIT_TEST_APPROX_EQUAL,
  APV_NUMERICAL_LIMIT_TESTS
  } apvNumericalLimitTests_t;

// Numerical overlay for variant numerical handling
typedef union apvNumericalVariant_tTag
  {
  int8_t        apv_int8_t;
  uint8_t       apv_uint8_t;
  char          apv_char_t;
  unsigned char apv_uchar_t;
  int16_t       apv_int16_t;
  uint16_t      apv_uint16_t;
  int16_t       apv_short_t;
  uint16_t      apv_ushort_t;
  uint32_t      apv_uint32_t;
  int32_t       apv_int32_t;
  int           apv_int_t;
  unsigned int  apv_uint_t;
#ifndef _APV_NUMERICAL_RANGE_ARM32_
  uint64_t      apv_uint64_t;
  int64_t       apv_int64_t;
#endif
  long          apv_long_t;
  unsigned long apv_ulong_t;
  float         apv_float_t;
#ifndef _APV_NUMERICAL_RANGE_ARM32_
  double        apv_double_t;
  long double   apv_long_double_t;
#endif
  } apvNumericalVariant_t;

/******************************************************************************/

#endif

/******************************************************************************/
/* ApvTypes.h                                                                 */
/******************************************************************************/
/* (C) PulsingCore Software Limited 2020                                      */
/******************************************************************************/
