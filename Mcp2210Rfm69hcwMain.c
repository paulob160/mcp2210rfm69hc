/******************************************************************************/
/* (C) PulsingCore Software Ltd 2020                                          */
/******************************************************************************/
/*                                                                            */
/* Mcp2210Rfm69HcwMain.c                                                      */
/* 23.07.2020                                                                 */
/* Paul O'Brien                                                               */
/*                                                                            */
/* - drivers and mainline functions for the MCP2210 <--> RFM69HCW interface   */
/* Ref : https://www.microchip.com/Developmenttools/ProductDetails/ADM00419#additional-summary */
/*                                                                            */
/******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <conio.h>
#include <windows.h>
#include "Mcp2210_Dev.h"
#include "Mcp2210Rfm69hcw.h"
#include "mcp2210_dll_um.h"
#include "apv_RFM69HCW_Misc.h"
#include "apv_RFM69HCW.h"

/******************************************************************************/
/* Constants :                                                                */
/******************************************************************************/

#define INITIAL_GPIO_PIN_SETTINGS  "Initial GPIO pin settings :"
#define MODIFIED_GPIO_PIN_SETTINGS "Modified GPIO pin settings :"

#define INITIAL_SPI_SETTINGS       "Initial SPI settings :"
#define MODIFIED_SPI_SETTINGS      "Modified SPI settings :"
#define RESET_SPI_SETTINGS         "Reset SPI settings :"

/******************************************************************************/
/* Local Variable Definitions :                                               */
/******************************************************************************/

int  deviceHandle   = 0,
    *deviceHandle_p = &deviceHandle;

Pio  ApvPeripheralLineControlBlock[APV_PERIPHERAL_LINE_GROUPS];    // shadow PIO control blocks
Pio *ApvPeripheralLineControlBlock_p[APV_PERIPHERAL_LINE_GROUPS] = // physical block addresses
  {
  APV_PIO_BLOCK_A,
  APV_PIO_BLOCK_B,
  APV_PIO_BLOCK_C,
  APV_PIO_BLOCK_D
  };

apvFIFOStructure_t apvSpi0ChipSelectFIFO[APV_SPI_FIXED_CHIP_SELECTS];

APV_TYPE_UINT      rfm69hcwActualTransferSize = 0;
APV_TYPE_UINT8     rfm69hcwRadioTemperature   = 0;

/******************************************************************************/
/* Local Function Declarations :                                              */
/******************************************************************************/

int __cdecl main(int argc, char *argv[]);

/******************************************************************************/
/* Local Function Definitions :                                               */
/******************************************************************************/

int __cdecl main(int argc, char *argv[])
  {
/******************************************************************************/

  Mcp2210DevErrorCodes_t           errorCodes                 = MCP2210_DEV_NO_ERROR;
                                   
  APV_TYPE_INT                     libraryVersionStringLength = 0,
                                   numberOfConnectedDevices   = 0;
                                   
  APV_TYPE_UINT8                   radioRegisterIndex         = 0;

  APV_TYPE_ULONG                   devicePathStringLength     = MCP2210_OPEN_DEVICE_PATH_STRING_MAXIMUM_LENGTH;

  apv_rfm69hcwRegisterAddressSet_t registerSet                = APV_RFM69HCW_REG_FIFO;

/******************************************************************************/

  system("clear");

  printf("\n MCP2210 USB-TO-SPI/GPIO DRIVER : ");
  printf("\n ------------------------------\n");

  errorCodes = Mcp2210DevInitialise(&Mcp2210DllLibraryVersion[0],
                                    &libraryVersionStringLength,
                                    &numberOfConnectedDevices);

  Mcp2210PrintBreak(1, 1, 80,'-');

  printf("\n Library Version : %ls\n", Mcp2210DllLibraryVersion);

  Mcp2210PrintBreak(1, 1, 80,'-');

  errorCodes = Mcp2210DeviceOpen((void *)&deviceHandle,
                                         &Mcp2210OpenDevicePath[0],
                                         &devicePathStringLength);

  /******************************************************************************/
  /* Read the initial GPIO configuration as a base for changes :                */
  /******************************************************************************/

  errorCodes = Mcp2210DevReadGpioConfig((void *)&deviceHandle,
                                                 MCP2210_VM_CONFIG,
                                                &Mcp2210GpioVmConfigurationRead.pinFunctions[0],
                                                &Mcp2210GpioVmConfigurationRead.pinOutputValues[0],
                                                &Mcp2210GpioVmConfigurationRead.pinDirections[0],
                                                &Mcp2210GpioVmConfigurationRead.remoteWakeupMode[0],
                                                &Mcp2210GpioVmConfigurationRead.interruptPulseCountMode[0],
                                                &Mcp2210GpioVmConfigurationRead.spiBusReleaseMode[0]);

  /******************************************************************************/
  /* Write the initial GPIO configuration for reference :                       */
  /******************************************************************************/

  Mcp2210PrintGpioConfiguration( INITIAL_GPIO_PIN_SETTINGS,
                                &Mcp2210GpioVmConfigurationRead);

  /******************************************************************************/
  /* Assign the initial settings as "write" settings for modification :         */
  /******************************************************************************/

  Mcp2210GpioVmConfigurationWrite = Mcp2210GpioVmConfigurationRead;

  /******************************************************************************/
  /* Set :                                                                      */
  /*       GPIO pin  #0      == SPI RESET                                       */
  /*       GPIO pin  #1      == SPI CS                                          */
  /*       GPIO pins #0 - #8 == SPI direction OUTPUT                            */
  /*       GPIO pins #0 - #8 == level HIGH                                      */
  /*       GPIO pins #0 - #8 == remote wakeup DISABLED                          */
  /*       GPIO pins #0 - #8 == bus release ENABLED                             */
  /******************************************************************************/

  Mcp2210GpioVmConfigurationWrite.pinFunctions[MCP2210_GPIO_PIN_0] = MCP2210_PIN_DES_GPIO;
  Mcp2210GpioVmConfigurationWrite.pinFunctions[MCP2210_GPIO_PIN_1] = MCP2210_PIN_DES_CS;

  Mcp2210GpioVmConfigurationWrite.pinDirections[0] = Mcp2210GpioVmConfigurationWrite.pinDirections[0] & (~(1 << MCP2210_GPIO_PIN_0));
  Mcp2210GpioVmConfigurationWrite.pinDirections[0] = Mcp2210GpioVmConfigurationWrite.pinDirections[0] & (~(1 << MCP2210_GPIO_PIN_1));
  Mcp2210GpioVmConfigurationWrite.pinDirections[0] = Mcp2210GpioVmConfigurationWrite.pinDirections[0] & (~(1 << MCP2210_GPIO_PIN_2));
  Mcp2210GpioVmConfigurationWrite.pinDirections[0] = Mcp2210GpioVmConfigurationWrite.pinDirections[0] & (~(1 << MCP2210_GPIO_PIN_3));
  Mcp2210GpioVmConfigurationWrite.pinDirections[0] = Mcp2210GpioVmConfigurationWrite.pinDirections[0] & (~(1 << MCP2210_GPIO_PIN_4));
  Mcp2210GpioVmConfigurationWrite.pinDirections[0] = Mcp2210GpioVmConfigurationWrite.pinDirections[0] & (~(1 << MCP2210_GPIO_PIN_5));
  Mcp2210GpioVmConfigurationWrite.pinDirections[0] = Mcp2210GpioVmConfigurationWrite.pinDirections[0] & (~(1 << MCP2210_GPIO_PIN_6));
  Mcp2210GpioVmConfigurationWrite.pinDirections[0] = Mcp2210GpioVmConfigurationWrite.pinDirections[0] & (~(1 << MCP2210_GPIO_PIN_7));
  Mcp2210GpioVmConfigurationWrite.pinDirections[0] = Mcp2210GpioVmConfigurationWrite.pinDirections[0] & (~(1 << MCP2210_GPIO_PIN_8));

  Mcp2210GpioVmConfigurationWrite.interruptPulseCountMode[0] = MCP2210_INT_MD_CNT_NONE;

  Mcp2210GpioVmConfigurationWrite.pinOutputValues[0]         = 0x000001FE; // RESET (GPIO #0) MUST BE "LOW" (found experimentally) !!!

  Mcp2210GpioVmConfigurationWrite.remoteWakeupMode[0]        = MCP2210_REMOTE_WAKEUP_DISABLED;
  Mcp2210GpioVmConfigurationWrite.spiBusReleaseMode[0]       = MCP2210_SPI_BUS_RELEASE_ENABLED;

  /******************************************************************************/
  /* Set the new configuration and repeat it back :                             */
  /******************************************************************************/

  errorCodes = Mcp2210DevWriteGpioConfig((void *)&deviceHandle,
                                          MCP2210_VM_CONFIG,
                                          &Mcp2210GpioVmConfigurationWrite.pinFunctions[0],
                                           Mcp2210GpioVmConfigurationWrite.pinOutputValues[0],
                                           Mcp2210GpioVmConfigurationWrite.pinDirections[0],
                                           Mcp2210GpioVmConfigurationWrite.remoteWakeupMode[0],
                                           Mcp2210GpioVmConfigurationWrite.interruptPulseCountMode[0],
                                           Mcp2210GpioVmConfigurationWrite.spiBusReleaseMode[0]);

  errorCodes = Mcp2210DevReadGpioConfig((void *)&deviceHandle,
                                        MCP2210_VM_CONFIG,
                                        &Mcp2210GpioVmConfigurationRead.pinFunctions[0],
                                        &Mcp2210GpioVmConfigurationRead.pinOutputValues[0],
                                        &Mcp2210GpioVmConfigurationRead.pinDirections[0],
                                        &Mcp2210GpioVmConfigurationRead.remoteWakeupMode[0],
                                        &Mcp2210GpioVmConfigurationRead.interruptPulseCountMode[0],
                                        &Mcp2210GpioVmConfigurationRead.spiBusReleaseMode[0]);

  Mcp2210PrintGpioConfiguration( MODIFIED_GPIO_PIN_SETTINGS,
                                &Mcp2210GpioVmConfigurationRead);

  /******************************************************************************/
  /* Read the initial SPI configuration as a basis for changes :                */
  /******************************************************************************/

  errorCodes = Mcp2210DevReadSpiConfig( (void *)&deviceHandle,
                                       MCP2210_VM_CONFIG,
                                      &Mcp2210SpiVmConfigurationRead.transferSpeed,
                                      &Mcp2210SpiVmConfigurationRead.chipSelectIdleLevel,
                                      &Mcp2210SpiVmConfigurationRead.chipSelectActiveLevel,
                                      &Mcp2210SpiVmConfigurationRead.csToFirstDataDelay,
                                      &Mcp2210SpiVmConfigurationRead.lastDataToCsDelay,
                                      &Mcp2210SpiVmConfigurationRead.intraDataToDataDelay,
                                      &Mcp2210SpiVmConfigurationRead.bytesPerTransfer,
                                      &Mcp2210SpiVmConfigurationRead.spiMode);

  /******************************************************************************/
  /* Write the initial SPI configuration for reference :                        */
  /******************************************************************************/

  Mcp2210DevPrintSpiConfiguration( INITIAL_SPI_SETTINGS,
                                  &Mcp2210SpiVmConfigurationRead);

  /******************************************************************************/
  /* Assign the initial settings as "write" settings for modification :         */
  /******************************************************************************/

  Mcp2210SpiVmConfigurationWrite = Mcp2210SpiVmConfigurationRead;

  /******************************************************************************/
  /* Set :                                                                      */
  /*  - "usual" register transfer size == 2 bytes (address + data)              */
  /*  - SPI mode                 == 0                                           */
  /*  - CS to first data delay   == 0                                           */
  /*  - last data to CS delay    == 0                                           */
  /*  - intra data-to-data delay == 0                                           */
  /*  - transfer speed           == 250kbps                                     */
  /******************************************************************************/

  // Set the transfer size
  Mcp2210SpiVmConfigurationRead.bytesPerTransfer     = APV_RFM69HCW_REGISTER_SETUP_MINIMUM_WIDTH;

  // Set the SPI mode
  Mcp2210SpiVmConfigurationRead.spiMode              = MCP2210_SPI_MODE_0;

  Mcp2210SpiVmConfigurationRead.chipSelectIdleLevel  = 0x02; // GPIO #1
  Mcp2210SpiVmConfigurationRead.chipSelectActiveLevel= 0x0;
  // MCP2210 delays are in quanta of 100usecs, the rfm69hcw in nsecs - leave all at 0 ?
  Mcp2210SpiVmConfigurationRead.csToFirstDataDelay   = 6;
  Mcp2210SpiVmConfigurationRead.lastDataToCsDelay    = 6;
  Mcp2210SpiVmConfigurationRead.intraDataToDataDelay = 0;

  // Set the transfer speed
  Mcp2210SpiVmConfigurationRead.transferSpeed        = APV_RFM69HCW_REGISTER_TRANSFER_SPEED;

  // Write the new configuration
  errorCodes = Mcp2210DevWriteSpiConfig( (void *)&deviceHandle,
                                          MCP2210_VM_CONFIG,
                                         Mcp2210SpiVmConfigurationRead.transferSpeed,
                                         Mcp2210SpiVmConfigurationRead.chipSelectIdleLevel,
                                         Mcp2210SpiVmConfigurationRead.chipSelectActiveLevel,
                                         Mcp2210SpiVmConfigurationRead.csToFirstDataDelay,
                                         Mcp2210SpiVmConfigurationRead.lastDataToCsDelay,
                                         Mcp2210SpiVmConfigurationRead.intraDataToDataDelay,
                                         Mcp2210SpiVmConfigurationRead.bytesPerTransfer,
                                         Mcp2210SpiVmConfigurationRead.spiMode);

  Mcp2210DevPrintSpiConfiguration( MODIFIED_SPI_SETTINGS,
                                  &Mcp2210SpiVmConfigurationRead);

  // Write the Rfm69Hcw register set defaults into their "shadows"
  for (registerSet = APV_RFM69HCW_REG_FIFO; registerSet < APV_RFM69HCW_REGISTER_ADDRESS_SET; registerSet++)
    {
    apvRfm69HcwWriteRegisterDefault(registerSet);
    }

  // Try to read the radio version number - RESET first
  apvMcp2210Rfm69HcwAtomicReset( (APV_TYPE_VOID *)&deviceHandle);

#if (0)
  errorCodes = Mcp2210DevReadGpioConfig((void *)&deviceHandle,
                                        MCP2210_VM_CONFIG,
                                        &Mcp2210GpioVmConfigurationRead.pinFunctions[0],
                                        &Mcp2210GpioVmConfigurationRead.pinOutputValues[0],
                                        &Mcp2210GpioVmConfigurationRead.pinDirections[0],
                                        &Mcp2210GpioVmConfigurationRead.remoteWakeupMode[0],
                                        &Mcp2210GpioVmConfigurationRead.interruptPulseCountMode[0],
                                        &Mcp2210GpioVmConfigurationRead.spiBusReleaseMode[0]);

  Mcp2210PrintGpioConfiguration( RESET_SPI_SETTINGS,
                                &Mcp2210GpioVmConfigurationRead);
#endif

  rfm69hcwActualTransferSize = APV_RFM69HCW_REGISTER_SETUP_MINIMUM_WIDTH;

  errorCodes = Mcp2210DevReadRfm69HcwRegister( APV_RFM69HCW_REG_VERSION,
                                              &apv_rfm69hcwRegisterDefinitions[0],
                                              &rfm69hcwRxDataBuffer[0],
                                              &rfm69hcwActualTransferSize,
                                               (APV_TYPE_VOID *)deviceHandle,
                                              &Mcp2210SpiVmConfigurationRead);

  if ((errorCodes == MCP2210_DEV_E_SUCCESS) && (rfm69hcwRxDataBuffer[APV_RFM69HCW_REGISTER_DATA_OFFSET] == APV_RFM69HCW_REGISTER_VERSION))
    {
    Mcp2210PrintBreak(1, 1, 80,'-');

    printf("\n RFM69HCW Register (RESET) Values : \n");

    // The radio has been found - read all the registers that can be read (but not the FIFO)
    for (radioRegisterIndex = APV_RFM69HCW_REG_OPMODE; radioRegisterIndex < APV_RFM69HCW_REGISTER_ADDRESS_SET; radioRegisterIndex++)
      {
      if ((apv_rfm69hcwRegisterDefinitions[radioRegisterIndex].apv_rfm69hcwRegisterInUse    == TRUE) &&
          (apv_rfm69hcwRegisterDefinitions[radioRegisterIndex].spv_rfm69hcwRegisterReadMask != ((APV_TYPE_UINT8)(~APV_RFM69HCW_REGISTER_DEFAULT_READ_MASK))))
        {
        errorCodes = Mcp2210DevReadRfm69HcwRegister( radioRegisterIndex,
                                                    &apv_rfm69hcwRegisterDefinitions[0],
                                                    &rfm69hcwRxDataBuffer[0],
                                                    &rfm69hcwActualTransferSize,
                                                     (APV_TYPE_VOID *)deviceHandle,
                                                    &Mcp2210SpiVmConfigurationRead);
        if (errorCodes == MCP2210_DEV_E_SUCCESS)
          {
          printf("\n %s = 0x%02x", apv_rfm69hcwRegisterNames[radioRegisterIndex], rfm69hcwRxDataBuffer[APV_RFM69HCW_REGISTER_DATA_OFFSET]);
          }
        else
          {
          printf("\n REGISTER READ ERROR!");
          }
        }
      }

    Mcp2210PrintBreak(1, 1, 80,'-');

    /******************************************************************************/
    /* WRITE THE TEMPERATURE MEASUREMENT BIT                                      */
    /* READ THE TEMPERATURE                                                       */
    /******************************************************************************/

    errorCodes = mcp2210ReadRfw69HcwTemperature((APV_TYPE_VOID *)deviceHandle,
                                                (APV_TYPE_UINT8 *)&rfm69hcwRadioTemperature);

    if (errorCodes == MCP2210_DEV_E_SUCCESS)
      {
      Mcp2210PrintBreak(1, 1, 80,'-');
      printf("\n Uncalibrated temperature = 0x%02x\n", rfm69hcwRadioTemperature);
      Mcp2210PrintBreak(1, 1, 80,'-');
      }
    else
      {
      Mcp2210PrintBreak(1, 1, 80,'-');
      printf("\n ERROR : temperature malfunction!\n");
      Mcp2210PrintBreak(1, 1, 80,'-');
      }

    // TEST
        Mcp2210PrintBreak(1, 1, 80,'-');

    printf("\n RFM69HCW Register (RESET) Values : \n");

    // The radio has been found - read all the registers that can be read (but not the FIFO)
    for (radioRegisterIndex = APV_RFM69HCW_REG_OPMODE; radioRegisterIndex < APV_RFM69HCW_REGISTER_ADDRESS_SET; radioRegisterIndex++)
      {
      if ((apv_rfm69hcwRegisterDefinitions[radioRegisterIndex].apv_rfm69hcwRegisterInUse    == TRUE) &&
          (apv_rfm69hcwRegisterDefinitions[radioRegisterIndex].spv_rfm69hcwRegisterReadMask != ((APV_TYPE_UINT8)(~APV_RFM69HCW_REGISTER_DEFAULT_READ_MASK))))
        {
        errorCodes = Mcp2210DevReadRfm69HcwRegister( radioRegisterIndex,
                                                    &apv_rfm69hcwRegisterDefinitions[0],
                                                    &rfm69hcwRxDataBuffer[0],
                                                    &rfm69hcwActualTransferSize,
                                                     (APV_TYPE_VOID *)deviceHandle,
                                                    &Mcp2210SpiVmConfigurationRead);
        if (errorCodes == MCP2210_DEV_E_SUCCESS)
          {
          printf("\n %s = 0x%02x", apv_rfm69hcwRegisterNames[radioRegisterIndex], rfm69hcwRxDataBuffer[APV_RFM69HCW_REGISTER_DATA_OFFSET]);
          }
        else
          {
          printf("\n REGISTER READ ERROR!");
          }
        }
      }

    Mcp2210PrintBreak(1, 1, 80,'-');

    // TEST
    /******************************************************************************/
    }
  else
    {
    Mcp2210PrintBreak(1, 1, 80,'-');
    printf("\n ERROR : radio malfunction!\n");
    Mcp2210PrintBreak(1, 1, 80,'-');
    }

/******************************************************************************/

  printf("\n Press any key to finish : \n");

  while (!_kbhit())
    ;

  Mcp2210_Reset((void *)&deviceHandle);

/******************************************************************************/

  return(0);

/******************************************************************************/
   } /* end of main                                                           */

/******************************************************************************/

APV_ERROR_CODE apvGetFIFOFillLevel(apvFIFOStructure_t *fifo,
                                   uint8_t            *fillLevel,
                                   bool                interruptControl)
  {
/******************************************************************************/

  APV_ERROR_CODE fifoError = APV_ERROR_CODE_NONE;

/******************************************************************************/
/******************************************************************************/

  return(fifoError);

  } /* end of apvGetFIFOFillLevel                                             */

/******************************************************************************/
/* (C) PulsingCore Software Ltd 2020                                          */
/******************************************************************************/