/******************************************************************************/
/* (C) PulsingCore Software 2020                                              */
/******************************************************************************/
/*                                                                            */
/* Mcp2210_Dev.c                                                              */
/* 23.07.20                                                                   */
/* Paul O'Brien                                                               */
/*                                                                            */
/* Ref : https://www.microchip.com/Developmenttools/ProductDetails/ADM00419#additional-summary */
/*                                                                            */
/******************************************************************************/
/* Include Files :                                                            */
/******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <conio.h>
#include <Windows.h>
#include <ctype.h>
#include "ApvTypes.h"
#include "Mcp2210_Dev.h"
#include "mcp2210_dll_um.h"

/******************************************************************************/
/* Global Variable Definitions :                                              */
/******************************************************************************/

wchar_t                      Mcp2210DllLibraryVersion[MPC2210_LIBRARY_VERSION_SIZE];
                             
wchar_t                      Mcp2210OpenDevicePath[MCP2210_OPEN_DEVICE_PATH_STRING_MAXIMUM_LENGTH];
                             
Mcp2210GpioConfiguration_t   Mcp2210GpioVmConfigurationWrite,
                             Mcp2210GpioVmConfigurationRead;

Mcp2210DevSpiConfiguration_t Mcp2210SpiVmConfigurationRead,
                             Mcp2210SpiVmConfigurationWrite;

APV_TYPE_UCHAR               rfm69hcwRxDataBuffer[APV_RFM69HCW_REGISTER_SETUP_MAXIMUM_WIDTH],
                             rfm69hcwTxDataBuffer[APV_RFM69HCW_REGISTER_SETUP_MAXIMUM_WIDTH],
                             rfm69hcwRegisterCommand[APV_RFM69HCW_REGISTER_SETUP_MAXIMUM_WIDTH];

/******************************************************************************/
/* Function Definitions :                                                     */
/******************************************************************************/
/* Mcp2210DevInitialise() :                                                   */
/******************************************************************************/

Mcp2210DevErrorCodes_t Mcp2210DevInitialise(wchar_t       *libraryVersionString,
                                            int           *libraryVersionStringLength,
                                            int           *numberOFConnectedDevices)
  {
/******************************************************************************/

  Mcp2210DevErrorCodes_t devErrorCodes = MCP2210_DEV_NO_ERROR;

/******************************************************************************/

  // Try and get a DLL library version
  *libraryVersionStringLength = Mcp2210_GetLibraryVersion(libraryVersionString);

  if (( ( Mcp2210DevErrorCodes_t )*libraryVersionStringLength ) == MCP2210_DEV_E_ERR_NULL)
    {
    devErrorCodes = MCP2210_DEV_E_ERR_NULL;
    }
  else
    {
    *numberOFConnectedDevices = Mcp2210_GetConnectedDevCount(MCP2210_DEFAULT_VID, MCP2210_DEFAULT_PID);

    if ((( ( Mcp2210DevErrorCodes_t )*numberOFConnectedDevices ) == MCP2210_DEV_E_ERR_UNKNOWN_ERROR) ||
        (( ( Mcp2210DevErrorCodes_t )*numberOFConnectedDevices ) == MCP2210_DEV_E_ERR_MALLOC))
      {
      devErrorCodes = MCP2210_DEV_E_ERR_NULL;
      }
    }

/******************************************************************************/

  return(devErrorCodes);

/******************************************************************************/
  } /* end of Mcp2210DevInitialise                                            */

/******************************************************************************/
/* Mcp2210DeviceOpen() :                                                      */
/*  --> deviceHandle_p : see below...                                         */
/*  --> devicePath     : a UNICODE string identifying the devices' enumerated */
/*                       id                                                   */
/*  --> devicePathStringLength : to persuade the function to fill the path    */
/*                               this must be passed as a non-zero value!     */
/*                                                                            */
/* - BEWARE!!! The function "Mcp2210_OpenByIndex" returns the device handle   */
/*             as an integer BUT assigns it AS IF it was a pointer address    */
/*             NOT the location the pointer points to! Hence the double       */
/*             indirection to correctly return the handle                     */
/*                                                                            */
/******************************************************************************/

Mcp2210DevErrorCodes_t Mcp2210DeviceOpen(void          **deviceHandle_p,
                                         wchar_t        *devicePath,
                                         unsigned long  *devicePathStringLength)
  {
/******************************************************************************/

  Mcp2210DevErrorCodes_t devErrorCodes =   MCP2210_DEV_NO_ERROR;

/******************************************************************************/

  *deviceHandle_p = Mcp2210_OpenByIndex(MCP2210_DEFAULT_VID, 
                                        MCP2210_DEFAULT_PID,
                                        MCP2210_DEVICE_INDEX_0,
                                        devicePath,
                                        devicePathStringLength);

  if (((Mcp2210DevErrorCodes_t)*deviceHandle_p) == MCP2210_DEV_E_ERR_UNKNOWN_ERROR) // on an invalid handle
    {
    devErrorCodes = (Mcp2210DevErrorCodes_t)Mcp2210_GetLastError();
    }

/******************************************************************************/

  return(devErrorCodes);

/******************************************************************************/
  } /* end of Mcp2210DeviceOpen                                               */

/******************************************************************************/
/* apvMcp2210Rfm69HcwAtomicReset() :                                          */
/*  --> apvCoreTimeBaseBlock : global timebase controlling structure          */
/*                                                                            */
/*  - carry out a chip RESET without yielding (as the "...Trigger" +          */
/*    "...Terminate"  pair do). The RESET needs :                             */
/*       (i) >100us assertion time                                            */
/*      (ii) > 5ms  dwell time after RESET                                    */
/*                                                                            */
/*  - RESET is a LOW -> HIGH -> LOW sequence wuth minimum timings             */
/*                                                                            */
/******************************************************************************/

APV_ERROR_CODE apvMcp2210Rfm69HcwAtomicReset(void **deviceHandle_p)
  {
/******************************************************************************/

  Mcp2210DevErrorCodes_t devErrorCodes  = MCP2210_DEV_NO_ERROR;

  APV_TYPE_UINT          chipBaseLevel  = 0,
                         chipResetLevel = 0;

/******************************************************************************/

  // Need to read the GPIO levels so the rest are preserved. NOTE THE VALUES 
  // ARE ONLY VALID FOR GPIO PINS, NOT CS OR FN!!!
  devErrorCodes = Mcp2210_GetGpioPinVal(*deviceHandle_p,
                                        &chipBaseLevel);

  // Firstly set RESET low and wait for (Windows minimum) of 1ms
  chipResetLevel = chipBaseLevel & ((APV_TYPE_UINT)(~APV_RFM69HCW_CHIP_RESET_MASK));

  chipResetLevel = chipResetLevel | APV_RFM69HCW_CHIP_RESET_LEVEL_LOW;

  devErrorCodes = Mcp2210_SetGpioPinVal(*deviceHandle_p,
                                         chipResetLevel,
                                        &chipBaseLevel);

  if ((chipResetLevel != chipBaseLevel) || (devErrorCodes != MCP2210_DEV_E_SUCCESS))
    {
    devErrorCodes = MCP2210_DEV_E_ERR_CMD_FAILED;
    }
  else
    {
    // Keep RESET low for > 150us
    Sleep(APV_RFM69HCW_CHIP_RESET_PRE_LOW);

    // Set RESET high for > 100us
    chipResetLevel = chipBaseLevel & ((APV_TYPE_UINT)(~APV_RFM69HCW_CHIP_RESET_MASK));
    
    chipResetLevel = chipResetLevel | APV_RFM69HCW_CHIP_RESET_LEVEL_HIGH;

    devErrorCodes = Mcp2210_SetGpioPinVal(*deviceHandle_p,
                                           chipResetLevel,
                                          &chipBaseLevel);
    
    if ((chipResetLevel != chipBaseLevel) || (devErrorCodes != MCP2210_DEV_E_SUCCESS))
      {
      devErrorCodes = MCP2210_DEV_E_ERR_CMD_FAILED;
      }
    else
      {
      // Keep RESET high for > 100us
      Sleep(APV_RFM69HCW_CHIP_RESET_HIGH);

      // Set RESET low for > 5ms
      chipResetLevel = chipBaseLevel & ((APV_TYPE_UINT)(~APV_RFM69HCW_CHIP_RESET_MASK));
      
      chipResetLevel = chipResetLevel | APV_RFM69HCW_CHIP_RESET_LEVEL_LOW;
      
      devErrorCodes = Mcp2210_SetGpioPinVal(*deviceHandle_p,
                                             chipResetLevel,
                                            &chipBaseLevel);

      if ((chipResetLevel != chipBaseLevel) || (devErrorCodes != MCP2210_DEV_E_SUCCESS))
        {
        devErrorCodes = MCP2210_DEV_E_ERR_CMD_FAILED;
        }
      else
        {
        // Keep RESET high for > 5ms
        Sleep(APV_RFM69HCW_CHIP_RESET_POST_LOW);
        }
      }
    }

/******************************************************************************/

  return(devErrorCodes);

/******************************************************************************/
  } /* end of apvMcp2210Rfm69HcwAtomicReset                                   */

/******************************************************************************/
/* Mcp2210DevReadGpioConfig() :                                               */
/*  --> deviceHandle_p : see "Mcp2210DeviceOpen"                              */
/*  --> chipSettingMode : [ MCP2210_VM_CONFIG | MCP2210_NVRAM_CONFIG ]        */
/*  <-- pinFunctions    : 1 { [ MCP2210_PIN_DES_GPIO | MCP2210_PIN_DES_CS |   */
/*                              MCP2210_PIN_DES_FN ] } 9                      */
/*  <-- pinOutputValues    : 1 { [ LOW == 0 | 1 == HIGH ] } 9 : BIT ARRAY     */
/*  <-- pinDirections      : 1 { [ IN == 0 | OUT == 1 ] } 9   : BIT ARRAY     */
/*  <-- remoteWakeupMode   : [ MCP2210_REMOTE_WAKEUP_DISABLED == 0 |          */
/*                             MCP2210_REMOTE_WAKEUP_ENABLED  == 1 ]          */
/*  <-- interruptPulseMode : [ MCP2210_INT_MD_CNT_NONE          == 0 |        */
/*                             MCP2210_INT_MD_CNT_FALLING_EDGES == 1 |        */
/*                             MCP2210_INT_MD_CNT_RISING_EDGES  == 2 |        */
/*                             MCP2210_INT_MD_CNT_LOW_PULSES    == 3 |        */
/*                             MCP2210_INT_MD_CNT_HIGH_PULSES   == 4 ]        */
/*  <-- spiBusReleaseMode  : [ MCP2210_SPI_BUS_RELEASE_ENABLED  == 0 |        */
/*                             MCP2210_SPI_BUS_RELEASE_DISABLED =- 1 ]        */
/*                                                                            */
/* Ref : MCP2210 DLL User Guide                                               */
/*  - see also https://godoc.org/github.com/faryon93/mcp2210.go               */
/*                                                                            */
/******************************************************************************/

Mcp2210DevErrorCodes_t Mcp2210DevReadGpioConfig(void          **deviceHandle_p,
                                                unsigned char   chipSettingMode,
                                                unsigned char  *pinFunctions,
                                                unsigned int   *pinOutputValues,
                                                unsigned int   *pinDirections,
                                                unsigned char  *remoteWakeupMode,
                                                unsigned char  *interruptPulseCountMode,
                                                unsigned char  *spiBusReleaseMode)
  {
/******************************************************************************/

  Mcp2210DevErrorCodes_t devErrorCodes = MCP2210_DEV_NO_ERROR;

/******************************************************************************/

  // A missing handle is not allowed!
  if (deviceHandle_p == NULL)
    {
    devErrorCodes = MCP2210_DEV_E_ERR_NULL;
    }
  else
    {
    // The setting mode is mandatory
    if (( chipSettingMode != MCP2210_VM_CONFIG ) && ( chipSettingMode != MCP2210_NVRAM_CONFIG))
      {
      // Default to VM
      chipSettingMode = MCP2210_VM_CONFIG;
      }

    // None of the setting report locations can be missing
    if (( pinFunctions     == NULL ) || ( pinOutputValues         == NULL ) || ( pinDirections     == NULL ) ||
        ( remoteWakeupMode == NULL ) || ( interruptPulseCountMode == NULL ) || ( spiBusReleaseMode == NULL ))
      {
      devErrorCodes = MCP2210_DEV_E_ERR_NULL;
      }
    else
      {
      devErrorCodes = (Mcp2210DevErrorCodes_t)Mcp2210_GetGpioConfig(*deviceHandle_p,
                                                                     chipSettingMode,
                                                                     pinFunctions,
                                                                     pinOutputValues,
                                                                     pinDirections,
                                                                     remoteWakeupMode,
                                                                     interruptPulseCountMode,
                                                                     spiBusReleaseMode);
      }
    }

/******************************************************************************/

  return(devErrorCodes);

/******************************************************************************/
  } /* end of Mcp2210DevReadGpioConfig                                        */

/******************************************************************************/
/* Mcp2210DevWriteGpioConfig()                                                */
/*  --> deviceHandle_p     : see "Mcp2210DeviceOpen"                          */
/*  --> chipSettingMode    : [ MCP2210_VM_CONFIG | MCP2210_NVRAM_CONFIG ]     */
/*  --> pinFunctions       : 1 { [ MCP2210_PIN_DES_GPIO | MCP2210_PIN_DES_CS  */
/*                               | MCP2210_PIN_DES_FN ] } 9                   */
/*  --> pinOutputValues    : 1 { [ LOW == 0 | 1 == HIGH ] } 9 : BIT ARRAY     */
/*  --> pinDirections      : 1 { [ IN == 0 | OUT == 1 ] } 9   : BIT ARRAY     */
/*  --> remoteWakeupMode   : [ MCP2210_REMOTE_WAKEUP_DISABLED == 0 |          */
/*                             MCP2210_REMOTE_WAKEUP_ENABLED  == 1 ]          */
/*  --> interruptPulseMode : [ MCP2210_INT_MD_CNT_NONE          == 0 |        */
/*                             MCP2210_INT_MD_CNT_FALLING_EDGES == 1 |        */
/*                             MCP2210_INT_MD_CNT_RISING_EDGES  == 2 |        */
/*                             MCP2210_INT_MD_CNT_LOW_PULSES    == 3 |        */
/*                             MCP2210_INT_MD_CNT_HIGH_PULSES   == 4 ]        */
/*  --> spiBusReleaseMode  : [ MCP2210_SPI_BUS_RELEASE_ENABLED  == 0 |        */
/*                             MCP2210_SPI_BUS_RELEASE_DISABLED =- 1 ]        */
/*                                                                            */
/* Ref : MCP2210 DLL User Guide                                               */
/*  - see also https://godoc.org/github.com/faryon93/mcp2210.go               */
/*                                                                            */
/******************************************************************************/

Mcp2210DevErrorCodes_t Mcp2210DevWriteGpioConfig(void          **deviceHandle_p,
                                                 unsigned char   chipSettingMode,
                                                 unsigned char  *pinFunctions,
                                                 unsigned int    pinOutputValues,
                                                 unsigned int    pinDirections,
                                                 unsigned char   remoteWakeupMode,
                                                 unsigned char   interruptPulseMode,
                                                 unsigned char   spiBusReleaseMode)
  {
/******************************************************************************/

  Mcp2210DevErrorCodes_t devErrorCodes =   MCP2210_DEV_NO_ERROR;

/******************************************************************************/

  // A missing handle is not allowed!
  if (deviceHandle_p == NULL)
    {
    devErrorCodes = MCP2210_DEV_E_ERR_NULL;
    }
  else
    {
    // The setting mode is mandatory
    if (( chipSettingMode != MCP2210_VM_CONFIG ) && ( chipSettingMode != MCP2210_NVRAM_CONFIG))
      {
      // Default to VRAM
      chipSettingMode = MCP2210_VM_CONFIG;
      } 

    if (pinFunctions == NULL)
      {
      devErrorCodes = MCP2210_DEV_E_ERR_NULL;
      }
    else
      {
      devErrorCodes = (Mcp2210DevErrorCodes_t)Mcp2210_SetGpioConfig(*deviceHandle_p,
                                                                     chipSettingMode,
                                                                     pinFunctions,
                                                                     pinOutputValues,
                                                                     pinDirections,
                                                                     remoteWakeupMode,
                                                                     interruptPulseMode,
                                                                     spiBusReleaseMode);
      }
    }

/******************************************************************************/

  return(devErrorCodes);

/******************************************************************************/
  } /* Mcp2210DevWriteGpioConfig                                              */


/******************************************************************************/
/* Mcp2210PrintGpioconfiguration() :                                          */
/*  --> gpioConfigurationLabel : [ title string | NULL ]                      */
/*  <-- gpioConfiguration      : GPIO pin configuration                       */
/******************************************************************************/

void Mcp2210PrintGpioConfiguration(char                       *gpioConfigurationLabel,
                                   Mcp2210GpioConfiguration_t *gpioConfiguration)
  {
/******************************************************************************/

           int gpioIndex = 0;

  unsigned int pinSet    = 0; // mask for the integer-based GPIO pin arrays

/******************************************************************************/

  if (gpioConfigurationLabel != NULL)
    {
    size_t labelLength = 0;

    printf("\n %s", gpioConfigurationLabel);
    printf("\n ");

    for (labelLength = 0; labelLength < strlen(gpioConfigurationLabel); labelLength++)
      {
      printf("-");
      }

    printf("\n");
    }

  pinSet = 1;

  for (gpioIndex = 0; gpioIndex < MCP2210_GPIO_NR; gpioIndex++)
    {
    printf("\n PIN[%+2d] : ", gpioIndex);

    printf("FUNCTION  : ");

    switch (gpioConfiguration->pinFunctions[gpioIndex])
      {
      case MCP2210_PIN_DES_GPIO : printf("GPIO    ");
                                  break;

      case MCP2210_PIN_DES_CS   : printf("CS      ");
                                  break;

      case MCP2210_PIN_DES_FN   : printf("FN      ");
                                  break;

      default                   : printf("UNKNOWN ");
                                  break;
      }

    printf("| LEVEL     : ");

    if (gpioConfiguration->pinOutputValues[0] & pinSet)
      {
      printf("HIGH ");
      }
    else
      {
      printf("LOW  ");
      }

 /*   switch (gpioConfiguration->pinOutputValues[gpioIndex])
      {
      case MCP2210_PIN_LEVEL_LOW  : printf("LOW     ");
                                    break;

      case MCP2210_PIN_LEVEL_HIGH : printf("HIGH    ");
                                    break;

      default                     : printf("UNKNOWN ");
                                    break;
      } */

    printf("| DIRECTION : ");

    if (gpioConfiguration->pinDirections[0] & pinSet)
      {
      printf("INPUT ");
      }
    else
      {
      printf("OUTPUT  ");
      }

    /* switch (gpioConfiguration->pinDirections[gpioIndex])
      {
      case MCP2210_PIN_DIR_IN  : printf("IN      ");
                                 break;

      case MCP2210_PIN_DIR_OUT : printf("OUT     ");
                                 break;

      default                  : printf("UNKNOWN ");
                                 break;
      } */

    printf("| WAKEUP : ");

    switch (gpioConfiguration->remoteWakeupMode[gpioIndex])
      {
      case MCP2210_REMOTE_WAKEUP_ENABLED  : printf("ENABLED  ");
                                            break; 

      case MCP2210_REMOTE_WAKEUP_DISABLED : printf("DISABLED ");
                                            break;

      default                             : printf("UNKNOWN ");
                                            break;
      }
    printf("| INTERRUPT MODE : ");

    switch (gpioConfiguration->interruptPulseCountMode[gpioIndex])
      {
      case MCP2210_INT_MD_CNT_NONE          : printf("NONE         ");
                                              break;

      case MCP2210_INT_MD_CNT_FALLING_EDGES : printf("FALLING EDGE ");
                                              break;

      case MCP2210_INT_MD_CNT_RISING_EDGES  : printf("RISING EDGE  ");
                                              break;

      case MCP2210_INT_MD_CNT_LOW_PULSES    : printf("LOW PULSE    ");
                                              break;

      case MCP2210_INT_MD_CNT_HIGH_PULSES   : printf("HIGH PULSE   ");
                                              break;

      default                               : printf("UNKNOWN      ");
                                              break;
      }

    printf("| BUS RELEASE : ");

    switch (gpioConfiguration->spiBusReleaseMode[gpioIndex])
      {
      case MCP2210_SPI_BUS_RELEASE_ENABLED  : printf("ENABLED  ");
                                              break;

      case MCP2210_SPI_BUS_RELEASE_DISABLED : printf("DISABLED ");
                                              break;

      default                               : printf("UNKNOWN  ");
                                              break;
      }

   
    pinSet = pinSet << 1;
    }

  printf("\n");

/******************************************************************************/
  } /* end of Mcp2210PrintGpioConfiguration                                   */

/******************************************************************************/
/* Mcp2210DevReadSpiConfig() :                                                */
/*  --> deviceHandle_p        : see "Mcp2210DeviceOpen"                       */
/*  --> chipSettingMode       : [ MCP2210_VM_CONFIG | MCP2210_NVRAM_CONFIG ]  */
/*  <-- transferSpeed         : bit rate                                      */
/*  <-- chipSelectIdleLevel   : bit-array of chip select idle levels -        */
/*                                  [ bit8 == CS8 .. bit 0 == CS0 ]           */
/*  <-- chipSelectActiveLevel : bit-array of chip select active levels -      */
/*                                  [ bit8 == CS8 .. bit 0 == CS0 ]           */
/*  <-- csToFirstDataDelay    : { 0 .. 65535 } * 100usec                      */
/*  <-- lastDataToCsDelay     : { 0 .. 65535 } * 100usec                      */
/*  <-- intraDataToDataDelay  : { 0 .. 65535 } * 100usec                      */
/*  <-- bytesPerTransfer      : { 0 .. 65535 }                                */
/*  <-- spiMode               : [ MCP2210_SPI_MODE0 | MCP2210_SPI_MODE1 |     */
/*                                MCP2210_SPI_MODE2 | MCP2210_SPI_MODE3 ]     */
/*                                                                            */
/* - read the current SPI transfer settings                                   */
/*                                                                            */
/******************************************************************************/

Mcp2210DevErrorCodes_t Mcp2210DevReadSpiConfig(void          **deviceHandle_p,
                                               unsigned char   chipSettingMode,
                                               unsigned int   *transferSpeed,
                                               unsigned int   *chipSelectIdleLevel,
                                               unsigned int   *chipSelectActiveLevel,
                                               unsigned int   *csToFirstDataDelay,
                                               unsigned int   *lastDataToCsDelay,
                                               unsigned int   *intraDataToDataDelay,
                                               unsigned int   *bytesPerTransfer,
                                               unsigned char  *spiMode)
  {
/******************************************************************************/

  Mcp2210DevErrorCodes_t devErrorCodes = MCP2210_DEV_NO_ERROR;

/******************************************************************************/

  // A missing handle is not allowed!
  if (deviceHandle_p == NULL)
    {
    devErrorCodes = MCP2210_DEV_E_ERR_NULL;
    }
  else
    {
    // The setting mode is mandatory
    if (( chipSettingMode != MCP2210_VM_CONFIG ) && ( chipSettingMode != MCP2210_NVRAM_CONFIG))
      {
      // Default to VRAM
      chipSettingMode = MCP2210_VM_CONFIG;
      }
    else
      {
      if (( transferSpeed      == NULL ) || ( chipSelectIdleLevel == NULL ) || ( chipSelectActiveLevel == NULL ) ||
          ( csToFirstDataDelay == NULL ) || ( lastDataToCsDelay   == NULL ) || (intraDataToDataDelay   == NULL)  ||
          ( bytesPerTransfer   == NULL ) || ( spiMode             == NULL ))
        {
        devErrorCodes = MCP2210_DEV_E_ERR_NULL;
        }
      else
        {
        devErrorCodes = Mcp2210_GetSpiConfig(*deviceHandle_p,
                                              chipSettingMode,
                                              transferSpeed,
                                              chipSelectIdleLevel,          
                                              chipSelectActiveLevel,          
                                              csToFirstDataDelay,
                                              lastDataToCsDelay,    
                                              intraDataToDataDelay,
                                              bytesPerTransfer,          
                                              spiMode);
        }
      }
    }

/******************************************************************************/

  return(devErrorCodes);

/******************************************************************************/
  } /* end of Mcp2210DevReadSpiConfig                                         */

/******************************************************************************/
/* Mcp2210DevWriteSpiConfig() :                                               */
/*  --> deviceHandle_p        : see "Mcp2210DeviceOpen"                       */
/*  --> chipSettingMode       : [ MCP2210_VM_CONFIG | MCP2210_NVRAM_CONFIG ]  */
/*  <-- transferSpeed         : bit rate                                      */
/*  <-- chipSelectIdleLevel   : bit-array of chip select idle levels -        */
/*                                  [ bit8 == CS8 .. bit 0 == CS0 ]           */
/*  <-- chipSelectActiveLevel : bit-array of chip select active levels -      */
/*                                  [ bit8 == CS8 .. bit 0 == CS0 ]           */
/*  <-- csToFirstDataDelay    : { 0 .. 65535 } * 100usec                      */
/*  <-- lastDataToCsDelay     : { 0 .. 65535 } * 100usec                      */
/*  <-- intraDataToDataDelay  : { 0 .. 65535 } * 100usec                      */
/*  <-- bytesPerTransfer      : { 0 .. 65535 }                                */
/*  <-- spiMode               : [ MCP2210_SPI_MODE0 | MCP2210_SPI_MODE1 |     */
/*                                MCP2210_SPI_MODE2 | MCP2210_SPI_MODE3 ]     */
/*                                                                            */
/* - write the new SPI transfer settings                                      */
/*                                                                            */
/******************************************************************************/

Mcp2210DevErrorCodes_t Mcp2210DevWriteSpiConfig(void          **deviceHandle_p,
                                                unsigned char   chipSettingMode,
                                                unsigned int    transferSpeed,
                                                unsigned int    chipSelectIdleLevel,
                                                unsigned int    chipSelectActiveLevel,
                                                unsigned int    csToFirstDataDelay,
                                                unsigned int    lastDataToCsDelay,
                                                unsigned int    intraDataToDataDelay,
                                                unsigned int    bytesPerTransfer,
                                                unsigned char   spiMode)
  {
/******************************************************************************/

  Mcp2210DevErrorCodes_t devErrorCodes = MCP2210_DEV_NO_ERROR;

/******************************************************************************/

  // A missing handle is not allowed!
  if (deviceHandle_p == NULL)
    {
    devErrorCodes = MCP2210_DEV_E_ERR_NULL;
    }
  else
    {
    // The setting mode is mandatory
    if (( chipSettingMode != MCP2210_VM_CONFIG ) && ( chipSettingMode != MCP2210_NVRAM_CONFIG))
      {
      // Default to VRAM
      chipSettingMode = MCP2210_VM_CONFIG;
      }
    else
      {
      devErrorCodes = Mcp2210_SetSpiConfig(*deviceHandle_p,
                                            chipSettingMode,
                                           &transferSpeed,
                                           &chipSelectIdleLevel,          
                                           &chipSelectActiveLevel,          
                                           &csToFirstDataDelay,
                                           &lastDataToCsDelay,    
                                           &intraDataToDataDelay,
                                           &bytesPerTransfer,          
                                           &spiMode);
      }
    }

/******************************************************************************/

  return(devErrorCodes);

/******************************************************************************/
  } /* end of Mcp2210DevWriteSpiConfig                                        */

/******************************************************************************/
/* Mcp2210DevWriteRfm69HcwRegister() :                                        */
/*  --> rfm69hcwRegister                 : rfm69hcw register number           */
/*  --> rfm69hcwRegisterDefinitions      : register characteristics           */
/*                                         definition array                   */
/*  <-- rfm69hcwTxRegisterCharacters     : array of bytes for the register    */
/*                                         DATA characters to write           */
/*  <-- rfm69hcwRxRegisterCharacters     : array of bytes for the register    */
/*                                         characters returned                */
/*  <-- rfm69hcwRegisterCharactersLength : number of register characters to   */
/*                                         be written/returned INCLUDING the  */
/*                                         address byte                       */
/*  --> mcp2210DeviceHandle              : the active MCP2210 device handle   */
/*  --> mcp2210SpiConfiguration          : the active MCP2210 SPI             */
/*                                         configuration                      */
/*                                                                            */
/* - write a rfm69hcw configuration register in the register map              */
/*                                                                            */
/******************************************************************************/

Mcp2210DevErrorCodes_t Mcp2210DevWriteRfm69HcwRegister(      apv_rfm69hcwRegisterAddressSet_t  rfm69HcwRegister,
                                                       const apv_rfm69hcwRegisterDefinition_t *rfm69hcwRegisterDefinitions,
                                                             APV_TYPE_UCHAR                   *rfm69hcwTxRegisterCharacters,
                                                             APV_TYPE_UCHAR                   *rfm69hcwRxRegisterCharacters,
                                                             APV_TYPE_UINT                    *rfm69hcwRegisterCharactersLength,
                                                             APV_TYPE_VOID                    *mcp2210DeviceHandle,
                                                             Mcp2210DevSpiConfiguration_t     *mcp2210SpiConfiguration)
  {
/******************************************************************************/

  Mcp2210DevErrorCodes_t devErrorCodes = MCP2210_DEV_NO_ERROR;

  APV_TYPE_UINT          bufferIndex   = 0;

/******************************************************************************/

  // Check the parameters ad nauseam
  if (( rfm69hcwRegisterDefinitions  == NULL ) || ( rfm69hcwTxRegisterCharacters     == NULL ) || 
      ( rfm69hcwRxRegisterCharacters == NULL ) || ( rfm69hcwRegisterCharactersLength == NULL ) || 
      ( mcp2210DeviceHandle          == NULL ) || ( mcp2210SpiConfiguration          == NULL ))
     {
     devErrorCodes = MCP2210_DEV_E_ERR_NULL;
     }
   else
     {
     if (rfm69HcwRegister >= APV_RFM69HCW_REGISTER_ADDRESS_SET)
       {
       devErrorCodes = MCP2210_DEV_E_ERR_INVALID_PARAMETER;
       }
     else
       {
       if ((rfm69hcwRegisterDefinitions + rfm69HcwRegister)->apv_rfm69hcwRegisterInUse == FALSE)
         {
         devErrorCodes = MCP2210_DEV_E_ERR_INVALID_PARAMETER;
         }
       else
         {
         if (*rfm69hcwRegisterCharactersLength > APV_RFM69HCW_REGISTER_SETUP_MAXIMUM_WIDTH)
           {
           devErrorCodes = MCP2210_DEV_E_ERR_INVALID_PARAMETER;
           }
         else
           {
           // Build the register write command : clear the transmit and receive buffers
           for (bufferIndex = APV_RFM69HCW_REGISTER_COMMAND_OFFSET; bufferIndex < *rfm69hcwRegisterCharactersLength; bufferIndex++)
             {
              rfm69hcwRegisterCommand[bufferIndex]         = APV_RFM69HCW_COMMAND_NOP;
             *(rfm69hcwRxRegisterCharacters + bufferIndex) = APV_RFM69HCW_COMMAND_NOP;
             }

           // Load the address byte with the write bit set
           rfm69hcwRegisterCommand[APV_RFM69HCW_REGISTER_COMMAND_OFFSET] = rfm69HcwRegister | APV_RFM69HCW_REGISTER_WRITE_DATA;

           // Load the data bytes
           for (bufferIndex = APV_RFM69HCW_REGISTER_DATA_OFFSET; bufferIndex < *rfm69hcwRegisterCharactersLength; bufferIndex++)
             {
             rfm69hcwRegisterCommand[bufferIndex] = *(rfm69hcwTxRegisterCharacters + (bufferIndex - 1));
             }

           // Now try to write the register at this address
           devErrorCodes = Mcp2210_xferSpiData( mcp2210DeviceHandle,
                                               &rfm69hcwRegisterCommand[0],
                                                rfm69hcwRxRegisterCharacters,
                                               &(mcp2210SpiConfiguration->transferSpeed),
                                                rfm69hcwRegisterCharactersLength,
                                                MCP2210_DEFAULT_CHIP_SELECT_MASK);
           }
         }
       }
     }

/******************************************************************************/

  return(devErrorCodes);

/******************************************************************************/
  } /* end of Mcp2210DevWriteRfm69HcwRegister                                 */

/******************************************************************************/
/* Mcp2210DevReadRfm69HcwRegister() :                                         */
/*  --> rfm69hcwRegister                 : rfm69hcw register number           */
/*  --> rfm69hcwRegisterDefinitions      : register characteristics           */
/*                                         definition array                   */
/*  <-- rfm69hcwRegisterCharacters       : array of bytes for the register    */
/*                                         characters returned                */
/*  <-- rfm69hcwRegisterCharactersLength : number of register characters to   */
/*                                         be written/returned INCLUDING the  */
/*                                         address byte                       */
/*  --> mcp2210DeviceHandle              : the active MCP2210 device handle   */
/*  --> mcp2210SpiConfiguration          : the active MCP2210 SPI             */
/*                                         configuration                      */
/*                                                                            */
/* - read a rfm69hcw configuration register in the register map               */
/*                                                                            */
/******************************************************************************/

Mcp2210DevErrorCodes_t Mcp2210DevReadRfm69HcwRegister(      apv_rfm69hcwRegisterAddressSet_t  rfm69HcwRegister,
                                                      const apv_rfm69hcwRegisterDefinition_t *rfm69hcwRegisterDefinitions,
                                                            APV_TYPE_UCHAR                   *rfm69hcwRegisterCharacters,
                                                            APV_TYPE_UINT                    *rfm69hcwRegisterCharactersLength,
                                                            APV_TYPE_VOID                    *mcp2210DeviceHandle,
                                                            Mcp2210DevSpiConfiguration_t     *mcp2210SpiConfiguration)
  {
/******************************************************************************/

  Mcp2210DevErrorCodes_t devErrorCodes = MCP2210_DEV_NO_ERROR;

  APV_TYPE_UINT          bufferIndex   = 0;

/******************************************************************************/

  // Check the parameters ad nauseam
  if (( rfm69hcwRegisterDefinitions      == NULL ) || ( rfm69hcwRegisterCharacters == NULL ) || 
      ( rfm69hcwRegisterCharactersLength == NULL ) || ( mcp2210DeviceHandle        == NULL ) ||
      ( mcp2210SpiConfiguration          == NULL ))
     {
     devErrorCodes = MCP2210_DEV_E_ERR_NULL;
     }
   else
     {
     if (rfm69HcwRegister >= APV_RFM69HCW_REGISTER_ADDRESS_SET)
       {
       devErrorCodes = MCP2210_DEV_E_ERR_INVALID_PARAMETER;
       }
     else
       {
       if ((rfm69hcwRegisterDefinitions + rfm69HcwRegister)->apv_rfm69hcwRegisterInUse == FALSE)
         {
         devErrorCodes = MCP2210_DEV_E_ERR_INVALID_PARAMETER;
         }
       else
         {
         if (*rfm69hcwRegisterCharactersLength > APV_RFM69HCW_REGISTER_SETUP_MAXIMUM_WIDTH)
           {
           devErrorCodes = MCP2210_DEV_E_ERR_INVALID_PARAMETER;
           }
         else
           {
           // Build the register read command : clear the transmit and receive buffers
           for (bufferIndex = 0; bufferIndex < *rfm69hcwRegisterCharactersLength; bufferIndex++)
             {
              rfm69hcwRegisterCommand[bufferIndex]       = APV_RFM69HCW_COMMAND_NOP;
             *(rfm69hcwRegisterCharacters + bufferIndex) = APV_RFM69HCW_COMMAND_NOP;
             }

           rfm69hcwRegisterCommand[APV_RFM69HCW_REGISTER_COMMAND_OFFSET] = rfm69HcwRegister | APV_RFM69HCW_REGISTER_READ_DATA;

           // Now try to read the register at this address
           devErrorCodes = Mcp2210_xferSpiData( mcp2210DeviceHandle,
                                               &rfm69hcwRegisterCommand[0],
                                                rfm69hcwRegisterCharacters,
                                               &(mcp2210SpiConfiguration->transferSpeed),
                                                rfm69hcwRegisterCharactersLength,
                                                MCP2210_DEFAULT_CHIP_SELECT_MASK);
           }
         }
       }
     }

/******************************************************************************/

  return(devErrorCodes);

/******************************************************************************/
  } /* end of Mcp2210DevReadRfm69HcwRegister                                  */

/******************************************************************************/
/* mcp2210ReadRfw69HcwTemperature() :                                         */
/*  --> radioTemperature : RFM69HCW chip temperature                          */
/******************************************************************************/

Mcp2210DevErrorCodes_t mcp2210ReadRfw69HcwTemperature(APV_TYPE_VOID  *mcp2210DeviceHandle,
                                                      APV_TYPE_UINT8 *radioTemperature)
  {
/******************************************************************************/

  Mcp2210DevErrorCodes_t devErrorCodes                = MCP2210_DEV_NO_ERROR;
                                                     
  APV_TYPE_UINT          actualTransferSize           = APV_RFM69HCW_REGISTER_SETUP_MINIMUM_WIDTH;

  APV_TYPE_UINT8         temperatureReadingInProgress = RF_TEMP1_MEAS_RUNNING;

/******************************************************************************/

  // Trigger a temperature reading
  rfm69hcwTxDataBuffer[APV_RFM69HCW_REGISTER_DATA_BUFFER_OFFSET] = RF_TEMP1_MEAS_START;

  devErrorCodes = Mcp2210DevWriteRfm69HcwRegister( APV_RFM69HCW_REG_TEMP1,
                                                 &apv_rfm69hcwRegisterDefinitions[0],
                                                 &rfm69hcwTxDataBuffer[0],
                                                 &rfm69hcwRxDataBuffer[0],
                                                 &actualTransferSize,
                                                  mcp2210DeviceHandle,
                                                 &Mcp2210SpiVmConfigurationRead);

  if (devErrorCodes == MCP2210_DEV_E_SUCCESS)
    {
    // Poll for the end of the temperature reading
    while ((temperatureReadingInProgress & RF_TEMP1_MEAS_RUNNING) == RF_TEMP1_MEAS_RUNNING)
      {
      devErrorCodes = Mcp2210DevReadRfm69HcwRegister( APV_RFM69HCW_REG_TEMP1,
                                                     &apv_rfm69hcwRegisterDefinitions[0],
                                                     &rfm69hcwRxDataBuffer[0],
                                                     &actualTransferSize,
                                                      mcp2210DeviceHandle,
                                                     &Mcp2210SpiVmConfigurationRead);

      if (devErrorCodes != MCP2210_DEV_E_SUCCESS)
        {
        break;
        }

      temperatureReadingInProgress = rfm69hcwRxDataBuffer[APV_RFM69HCW_REGISTER_DATA_OFFSET];
      }
    }

  // Read the chip temperature
  devErrorCodes = Mcp2210DevReadRfm69HcwRegister( APV_RFM69HCW_REG_TEMP2,
                                                 &apv_rfm69hcwRegisterDefinitions[0],
                                                 &rfm69hcwRxDataBuffer[0],
                                                 &actualTransferSize,
                                                  mcp2210DeviceHandle,
                                                 &Mcp2210SpiVmConfigurationRead);

  if (devErrorCodes == MCP2210_DEV_E_SUCCESS)
    {
    *radioTemperature = rfm69hcwRxDataBuffer[APV_RFM69HCW_REGISTER_DATA_OFFSET];
    }


  /* TEST*/
#if (0)
  /******************************************************************************/
  /* Write the RF centre-frequency as three seperate transactions : MSB, MID    */
  /* and LSB. By experimentation : to change the MSB and/or MID frequency       */
  /* settings the LSB MUST be written last. MSB and MID write-order does not    */
  /* matter                                                                     */
  /******************************************************************************/

#if (1)
  rfm69hcwTxDataBuffer[0] = 0xE1;

  devErrorCodes = Mcp2210DevWriteRfm69HcwRegister( APV_RFM69HCW_REG_FRFMSB,
                                                 &apv_rfm69hcwRegisterDefinitions[0],
                                                 &rfm69hcwTxDataBuffer[0],
                                                 &rfm69hcwRxDataBuffer[0],
                                                 &actualTransferSize,
                                                  mcp2210DeviceHandle,
                                                 &Mcp2210SpiVmConfigurationRead);
#endif

#if (1)
  rfm69hcwTxDataBuffer[0] = 0xBE;

  devErrorCodes = Mcp2210DevWriteRfm69HcwRegister( APV_RFM69HCW_REG_FRFMID,
                                                &apv_rfm69hcwRegisterDefinitions[0],
                                                &rfm69hcwTxDataBuffer[0],
                                                &rfm69hcwRxDataBuffer[0],
                                                &actualTransferSize,
                                                 mcp2210DeviceHandle,
                                                &Mcp2210SpiVmConfigurationRead);
#endif

#if (1)
  rfm69hcwTxDataBuffer[0] = 0x4B;

  devErrorCodes = Mcp2210DevWriteRfm69HcwRegister( APV_RFM69HCW_REG_FRFLSB,
                                                 &apv_rfm69hcwRegisterDefinitions[0],
                                                 &rfm69hcwTxDataBuffer[0],
                                                 &rfm69hcwRxDataBuffer[0],
                                                 &actualTransferSize,
                                                  mcp2210DeviceHandle,
                                                 &Mcp2210SpiVmConfigurationRead);
#endif
#else
  /******************************************************************************/
  /* Write the RF centre-frequency as one transaction in the order MSB, MID and */
  /* and LSB. The rfm69hcw SPI slave automatically indexes the register (byte)  */
  /* address of each consecutive data byte, ending the transaction as usual     */
  /* when CS goes high                                                          */
  /******************************************************************************/

  actualTransferSize = 4;
  rfm69hcwTxDataBuffer[0] = 0xDE;
  rfm69hcwTxDataBuffer[1] = 0xBD;
  rfm69hcwTxDataBuffer[2] = 0x4B;

  devErrorCodes = Mcp2210DevWriteRfm69HcwRegister( APV_RFM69HCW_REG_FRFMSB,
                                                 &apv_rfm69hcwRegisterDefinitions[0],
                                                 &rfm69hcwTxDataBuffer[0],
                                                 &rfm69hcwRxDataBuffer[0],
                                                 &actualTransferSize,
                                                  mcp2210DeviceHandle,
                                                 &Mcp2210SpiVmConfigurationRead);
#endif

  devErrorCodes = Mcp2210DevReadRfm69HcwRegister( APV_RFM69HCW_REG_FRFMSB,
                                                 &apv_rfm69hcwRegisterDefinitions[0],
                                                 &rfm69hcwRxDataBuffer[0],
                                                 &actualTransferSize,
                                                  mcp2210DeviceHandle,
                                                 &Mcp2210SpiVmConfigurationRead);

/******************************************************************************/

  return(devErrorCodes);

/******************************************************************************/
  } /* end of mcp2210ReadRfw69HcwTemperature                                  */

/******************************************************************************/
/* Mcp2210DevPrintSpiConfiguration() :                                        */
/*  --> spiConfigurationLabel : a title to print or NULL                      */
/*  --> spiConfiguration      : the current NVRAM or VRAM SPI configuration   */
/******************************************************************************/

void Mcp2210DevPrintSpiConfiguration(char                         *spiConfigurationLabel,
                                     Mcp2210DevSpiConfiguration_t *spiConfiguration)
  {
/******************************************************************************/

  int csIndex = 0,
      csMask  = 1;

/******************************************************************************/

  if (spiConfiguration != NULL)
    {
    if (spiConfigurationLabel != NULL)
      {
      size_t labelLength = 0;

      printf("\n %s", spiConfigurationLabel);
      printf("\n ");

      for (labelLength = 0; labelLength < strlen(spiConfigurationLabel); labelLength++)
        {
        printf("-");
        }

      printf("\n");
      }

    printf("\n SPI CONFIGURATION : ");
    printf("\n -----------------\n");

    printf("\n Transfer Speed = %d ", spiConfiguration->transferSpeed);

    printf("\n Chip Select Idle Levels   : ");

    for (csIndex = 0; csIndex < MCP2210_CHIP_SELECTS; csIndex++)
      {
      printf("CS[%1d] == %1d : ", csIndex, (spiConfiguration->chipSelectIdleLevel & csMask) >> csIndex);

      csMask = csMask << 1;
      }

    printf("\n Chip Select Active Levels : ");

    csMask = 1;

    for (csIndex = 0; csIndex < MCP2210_CHIP_SELECTS; csIndex++)
      {
      printf("CS[%1d] == %1d : ", csIndex, (spiConfiguration->chipSelectActiveLevel & csMask) >> csIndex);

      csMask = csMask << 1;
      }

    printf("\n Chip-select to first data delay = %8d us", spiConfiguration->csToFirstDataDelay   * MCP2210_DATA_DELAY_QUANTUM);

    printf("\n Last data to chip-select delay  = %8d us", spiConfiguration->lastDataToCsDelay    * MCP2210_DATA_DELAY_QUANTUM);

    printf("\n Intra-data delay                = %8d us", spiConfiguration->intraDataToDataDelay * MCP2210_DATA_DELAY_QUANTUM);

    printf("\n Byte transfer size              = %5d   ", spiConfiguration->bytesPerTransfer);

    printf("\n SPI Mode = ");

    switch (spiConfiguration->spiMode)
      {
      case MCP2210_SPI_MODE_0 : printf("MCP2210_SPI_MODE_0");
                                break;

      case MCP2210_SPI_MODE_1 : printf("MCP2210_SPI_MODE_1");
                                break;

      case MCP2210_SPI_MODE_2 : printf("MCP2210_SPI_MODE_2");
                                break;

      case MCP2210_SPI_MODE_3 : printf("MCP2210_SPI_MODE_3");
                                break;

      default                 : printf("No such mode!");
                                break;
      }

    printf("\n");
    }

/******************************************************************************/
  } /* end of Mcp2210DevPrintSpiConfiguration                                 */

/******************************************************************************/
/* Mcp2210PrintBreak() :                                                      */
/*  --> leadingBlankLines   : number of blank lines to insert before the      */
/*                            break line                                      */
/*  --> followingBlankLines : number of blank lines to insert after the break */
/*                            line                                            */
/*  --> breakLineLength     : number of characters to print for the break     */
/*  --> breakCharacter      : character to print as break                     */
/*                                                                            */
/* - insert a "break" sequence in screen diagnostic output. Arbitrary limits  */
/*   are imposed for the break components.  Only printable characters are     */
/*   allowed for the break character which defaults to '*'                    */
/*                                                                            */
/******************************************************************************/

void Mcp2210PrintBreak(size_t leadingBlankLines,
                       size_t followingBlankLines,
                       size_t breakLineLength,
                       char   breakCharacter)
  {
/******************************************************************************/          

  // Insert the leading blank lines (if any) :
  if (leadingBlankLines > MCP2210_BREAK_MAXIMUM_LEADING_BLANK_LINES)
    {
    leadingBlankLines = MCP2210_BREAK_MAXIMUM_LEADING_BLANK_LINES;
    }

  while (leadingBlankLines > 0)
    {
    printf("\n");

    leadingBlankLines = leadingBlankLines - 1;
    }

/******************************************************************************/

  // Insert the break line characters (if any) : check for printables
  if (!isgraph(breakCharacter))
    {
    breakCharacter = MCP2210_BREAK_DEFAULT_CHARACTER;
    }

  if (breakLineLength > MCP2210_BREAK_MAXIMUM_LINE_LENGTH)
    {
    breakLineLength = MCP2210_BREAK_MAXIMUM_LINE_LENGTH;
    }

  if (breakLineLength > 0)
    {
    printf("\n");
    
    while (breakLineLength > 0)
      {
      printf("%c", breakCharacter);
    
      breakLineLength = breakLineLength - 1;
      }
    
    printf("\n");
    }

/******************************************************************************/

  // Insert the leading blank lines (if any) :
  if (followingBlankLines > MCP2210_BREAK_MAXIMUM_FOLLOWING_BLANK_LINES)
    {
    followingBlankLines = MCP2210_BREAK_MAXIMUM_FOLLOWING_BLANK_LINES;
    }

  while (followingBlankLines > 0)
    {
    printf("\n");

    followingBlankLines = followingBlankLines - 1;
    }

/******************************************************************************/
  } /* end of Mcp2210PrintBreak                                               */

/******************************************************************************/
/* (C) PulsingCore Software 2020                                              */
/******************************************************************************/