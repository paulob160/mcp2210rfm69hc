/******************************************************************************/
/* (C) PulsingCoreSoftware Limited 2020 (C)                                   */
/******************************************************************************/
/*                                                                            */
/* apv_RFM69HCW_Misc.h                                                        */
/* 05.08.20                                                                   */
/* POB                                                                        */
/*                                                                            */
/* - miscellaneous parts of the RFM69HCW Arduino code                         */
/*                                                                            */
/******************************************************************************/

#ifndef _APV_RFM69HCW_MISC_H_
#define _APV_RFM69HCW_MISC_H_

/******************************************************************************/

#include "stdint.h"
#include "stdbool.h"
#include "ApvError.h"

/******************************************************************************/

#define APV_CORE_TIMER_DURATION_TIMERS          16                     // this is enough ? Do not want to use "malloc()"!
#define APV_DURATION_TIMER_NULL_INDEX           ((uint32_t)~0)

#define APV_EVENT_TIMER_INVERSE_NANOSECONDS     ((uint64_t)1000000000) // one-second in nanoseconds

#define PIO_CODR_P0                             (0x1u << 0)
#define PIO_SODR_P0                             (0x1u << 0)
#define PIO_OER_P0                              (0x1u << 0)
#define PIO_ODR_P0                              (0x1u << 0)

// The physical addresses of the four peripheral I/O controller blocks
#define APV_PIO_BLOCK_A ((Pio *)0x400E0E00U)
#define APV_PIO_BLOCK_B ((Pio *)0x400E1000U)
#define APV_PIO_BLOCK_C ((Pio *)0x400E1200U)
#define APV_PIO_BLOCK_D ((Pio *)0x400E1400U)

#define __O
#define __I
#define __IO

/******************************************************************************/
/* Constant Equivalences :                                                    */
/******************************************************************************/
// Equating Visual C++'s poor treatment of "booleans'
#ifndef FALSE
#define FALSE false
#endif
#ifndef TRUE
#define TRUE  true
#endif

/******************************************************************************/

/** \brief Pio hardware registers */
typedef struct {
  __O  uint32_t PIO_PER;       /**< \brief (Pio Offset: 0x0000) PIO Enable Register */
  __O  uint32_t PIO_PDR;       /**< \brief (Pio Offset: 0x0004) PIO Disable Register */
  __I  uint32_t PIO_PSR;       /**< \brief (Pio Offset: 0x0008) PIO Status Register */
  __I  uint32_t Reserved1[1];
  __O  uint32_t PIO_OER;       /**< \brief (Pio Offset: 0x0010) Output Enable Register */
  __O  uint32_t PIO_ODR;       /**< \brief (Pio Offset: 0x0014) Output Disable Register */
  __I  uint32_t PIO_OSR;       /**< \brief (Pio Offset: 0x0018) Output Status Register */
  __I  uint32_t Reserved2[1];
  __O  uint32_t PIO_IFER;      /**< \brief (Pio Offset: 0x0020) Glitch Input Filter Enable Register */
  __O  uint32_t PIO_IFDR;      /**< \brief (Pio Offset: 0x0024) Glitch Input Filter Disable Register */
  __I  uint32_t PIO_IFSR;      /**< \brief (Pio Offset: 0x0028) Glitch Input Filter Status Register */
  __I  uint32_t Reserved3[1];
  __O  uint32_t PIO_SODR;      /**< \brief (Pio Offset: 0x0030) Set Output Data Register */
  __O  uint32_t PIO_CODR;      /**< \brief (Pio Offset: 0x0034) Clear Output Data Register */
  __IO uint32_t PIO_ODSR;      /**< \brief (Pio Offset: 0x0038) Output Data Status Register */
  __I  uint32_t PIO_PDSR;      /**< \brief (Pio Offset: 0x003C) Pin Data Status Register */
  __O  uint32_t PIO_IER;       /**< \brief (Pio Offset: 0x0040) Interrupt Enable Register */
  __O  uint32_t PIO_IDR;       /**< \brief (Pio Offset: 0x0044) Interrupt Disable Register */
  __I  uint32_t PIO_IMR;       /**< \brief (Pio Offset: 0x0048) Interrupt Mask Register */
  __I  uint32_t PIO_ISR;       /**< \brief (Pio Offset: 0x004C) Interrupt Status Register */
  __O  uint32_t PIO_MDER;      /**< \brief (Pio Offset: 0x0050) Multi-driver Enable Register */
  __O  uint32_t PIO_MDDR;      /**< \brief (Pio Offset: 0x0054) Multi-driver Disable Register */
  __I  uint32_t PIO_MDSR;      /**< \brief (Pio Offset: 0x0058) Multi-driver Status Register */
  __I  uint32_t Reserved4[1];
  __O  uint32_t PIO_PUDR;      /**< \brief (Pio Offset: 0x0060) Pull-up Disable Register */
  __O  uint32_t PIO_PUER;      /**< \brief (Pio Offset: 0x0064) Pull-up Enable Register */
  __I  uint32_t PIO_PUSR;      /**< \brief (Pio Offset: 0x0068) Pad Pull-up Status Register */
  __I  uint32_t Reserved5[1];
  __IO uint32_t PIO_ABSR;      /**< \brief (Pio Offset: 0x0070) Peripheral AB Select Register */
  __I  uint32_t Reserved6[3];
  __O  uint32_t PIO_SCIFSR;    /**< \brief (Pio Offset: 0x0080) System Clock Glitch Input Filter Select Register */
  __O  uint32_t PIO_DIFSR;     /**< \brief (Pio Offset: 0x0084) Debouncing Input Filter Select Register */
  __I  uint32_t PIO_IFDGSR;    /**< \brief (Pio Offset: 0x0088) Glitch or Debouncing Input Filter Clock Selection Status Register */
  __IO uint32_t PIO_SCDR;      /**< \brief (Pio Offset: 0x008C) Slow Clock Divider Debouncing Register */
  __I  uint32_t Reserved7[4];
  __O  uint32_t PIO_OWER;      /**< \brief (Pio Offset: 0x00A0) Output Write Enable */
  __O  uint32_t PIO_OWDR;      /**< \brief (Pio Offset: 0x00A4) Output Write Disable */
  __I  uint32_t PIO_OWSR;      /**< \brief (Pio Offset: 0x00A8) Output Write Status Register */
  __I  uint32_t Reserved8[1];
  __O  uint32_t PIO_AIMER;     /**< \brief (Pio Offset: 0x00B0) Additional Interrupt Modes Enable Register */
  __O  uint32_t PIO_AIMDR;     /**< \brief (Pio Offset: 0x00B4) Additional Interrupt Modes Disables Register */
  __I  uint32_t PIO_AIMMR;     /**< \brief (Pio Offset: 0x00B8) Additional Interrupt Modes Mask Register */
  __I  uint32_t Reserved9[1];
  __O  uint32_t PIO_ESR;       /**< \brief (Pio Offset: 0x00C0) Edge Select Register */
  __O  uint32_t PIO_LSR;       /**< \brief (Pio Offset: 0x00C4) Level Select Register */
  __I  uint32_t PIO_ELSR;      /**< \brief (Pio Offset: 0x00C8) Edge/Level Status Register */
  __I  uint32_t Reserved10[1];
  __O  uint32_t PIO_FELLSR;    /**< \brief (Pio Offset: 0x00D0) Falling Edge/Low Level Select Register */
  __O  uint32_t PIO_REHLSR;    /**< \brief (Pio Offset: 0x00D4) Rising Edge/ High Level Select Register */
  __I  uint32_t PIO_FRLHSR;    /**< \brief (Pio Offset: 0x00D8) Fall/Rise - Low/High Status Register */
  __I  uint32_t Reserved11[1];
  __I  uint32_t PIO_LOCKSR;    /**< \brief (Pio Offset: 0x00E0) Lock Status */
  __IO uint32_t PIO_WPMR;      /**< \brief (Pio Offset: 0x00E4) Write Protect Mode Register */
  __I  uint32_t PIO_WPSR;      /**< \brief (Pio Offset: 0x00E8) Write Protect Status Register */
} Pio;

/** \brief Spi hardware registers */
typedef struct {
  __O  uint32_t SPI_CR;        /**< \brief (Spi Offset: 0x00) Control Register */
  __IO uint32_t SPI_MR;        /**< \brief (Spi Offset: 0x04) Mode Register */
  __I  uint32_t SPI_RDR;       /**< \brief (Spi Offset: 0x08) Receive Data Register */
  __O  uint32_t SPI_TDR;       /**< \brief (Spi Offset: 0x0C) Transmit Data Register */
  __I  uint32_t SPI_SR;        /**< \brief (Spi Offset: 0x10) Status Register */
  __O  uint32_t SPI_IER;       /**< \brief (Spi Offset: 0x14) Interrupt Enable Register */
  __O  uint32_t SPI_IDR;       /**< \brief (Spi Offset: 0x18) Interrupt Disable Register */
  __I  uint32_t SPI_IMR;       /**< \brief (Spi Offset: 0x1C) Interrupt Mask Register */
  __I  uint32_t Reserved1[4];
  __IO uint32_t SPI_CSR[4];    /**< \brief (Spi Offset: 0x30) Chip Select Register */
  __I  uint32_t Reserved2[41];
  __IO uint32_t SPI_WPMR;      /**< \brief (Spi Offset: 0xE4) Write Protection Control Register */
  __I  uint32_t SPI_WPSR;      /**< \brief (Spi Offset: 0xE8) Write Protection Status Register */
} Spi;

#define ID_SUPC   ( 0) /**< \brief Supply Controller (SUPC) */
#define ID_RSTC   ( 1) /**< \brief Reset Controller (RSTC) */
#define ID_RTC    ( 2) /**< \brief Real Time Clock (RTC) */
#define ID_RTT    ( 3) /**< \brief Real Time Timer (RTT) */
#define ID_WDT    ( 4) /**< \brief Watchdog Timer (WDT) */
#define ID_PMC    ( 5) /**< \brief Power Management Controller (PMC) */
#define ID_EFC0   ( 6) /**< \brief Enhanced Flash Controller 0 (EFC0) */
#define ID_EFC1   ( 7) /**< \brief Enhanced Flash Controller 1 (EFC1) */
#define ID_UART   ( 8) /**< \brief Universal Asynchronous Receiver Transceiver (UART) */
#define ID_SMC    ( 9) /**< \brief Static Memory Controller (SMC) */
#define ID_PIOA   (11) /**< \brief Parallel I/O Controller A, (PIOA) */
#define ID_PIOB   (12) /**< \brief Parallel I/O Controller B (PIOB) */
#define ID_PIOC   (13) /**< \brief Parallel I/O Controller C (PIOC) */
#define ID_PIOD   (14) /**< \brief Parallel I/O Controller D (PIOD) */
#define ID_USART0 (17) /**< \brief USART 0 (USART0) */
#define ID_USART1 (18) /**< \brief USART 1 (USART1) */
#define ID_USART2 (19) /**< \brief USART 2 (USART2) */
#define ID_USART3 (20) /**< \brief USART 3 (USART3) */
#define ID_HSMCI  (21) /**< \brief Multimedia Card Interface (HSMCI) */
#define ID_TWI0   (22) /**< \brief Two-Wire Interface 0 (TWI0) */
#define ID_TWI1   (23) /**< \brief Two-Wire Interface 1 (TWI1) */
#define ID_SPI0   (24) /**< \brief Serial Peripheral Interface (SPI0) */
#define ID_SSC    (26) /**< \brief Synchronous Serial Controller (SSC) */
#define ID_TC0    (27) /**< \brief Timer Counter 0 (TC0) */
#define ID_TC1    (28) /**< \brief Timer Counter 1 (TC1) */
#define ID_TC2    (29) /**< \brief Timer Counter 2 (TC2) */
#define ID_TC3    (30) /**< \brief Timer Counter 3 (TC3) */
#define ID_TC4    (31) /**< \brief Timer Counter 4 (TC4) */
#define ID_TC5    (32) /**< \brief Timer Counter 5 (TC5) */
#define ID_TC6    (33) /**< \brief Timer Counter 6 (TC6) */
#define ID_TC7    (34) /**< \brief Timer Counter 7 (TC7) */
#define ID_TC8    (35) /**< \brief Timer Counter 8 (TC8) */
#define ID_PWM    (36) /**< \brief Pulse Width Modulation Controller (PWM) */
#define ID_ADC    (37) /**< \brief ADC Controller (ADC) */
#define ID_DACC   (38) /**< \brief DAC Controller (DACC) */
#define ID_DMAC   (39) /**< \brief DMA Controller (DMAC) */
#define ID_UOTGHS (40) /**< \brief USB OTG High Speed (UOTGHS) */
#define ID_TRNG   (41) /**< \brief True Random Number Generator (TRNG) */
#define ID_EMAC   (42) /**< \brief Ethernet MAC (EMAC) */
#define ID_CAN0   (43) /**< \brief CAN Controller 0 (CAN0) */
#define ID_CAN1   (44) /**< \brief CAN Controller 1 (CAN1) */

/******************************************************************************/
/* Digital I/O Pin Allocation :                                               */
/******************************************************************************/

typedef enum apvPeripheralId_tTag
  {
  APV_PERIPHERAL_ID_SUPC   = ID_SUPC,
  APV_PERIPHERAL_ID_RSTC   = ID_RSTC,
  APV_PERIPHERAL_ID_RTC    = ID_RTC,
  APV_PERIPHERAL_ID_RTT    = ID_RTT,
  APV_PERIPHERAL_ID_WDT    = ID_WDT,
  APV_PERIPHERAL_ID_PMC    = ID_PMC,
  APV_PERIPHERAL_ID_EFC0   = ID_EFC0,
  APV_PERIPHERAL_ID_EFC1   = ID_EFC1,
  APV_PERIPHERAL_ID_UART   = ID_UART,
  APV_PERIPHERAL_ID_SMC    = ID_SMC,
  APV_PERIPHERAL_ID_PIOA   = ID_PIOA,
  APV_PERIPHERAL_ID_PIOB   = ID_PIOB,
  APV_PERIPHERAL_ID_PIOC   = ID_PIOC,
  APV_PERIPHERAL_ID_PIOD   = ID_PIOD,
  APV_PERIPHERAL_ID_USART0 = ID_USART0,
  APV_PERIPHERAL_ID_USART1 = ID_USART1,
  APV_PERIPHERAL_ID_USART2 = ID_USART2,
  APV_PERIPHERAL_ID_USART3 = ID_USART3,
  APV_PERIPHERAL_ID_HSMCI  = ID_HSMCI,
  APV_PERIPHERAL_ID_TWI0   = ID_TWI0,
  APV_PERIPHERAL_ID_TWI1   = ID_TWI1,
  APV_PERIPHERAL_ID_SPI0   = ID_SPI0,
  APV_PERIPHERAL_ID_SSC    = ID_SSC,
  APV_PERIPHERAL_ID_TC0    = ID_TC0,
  APV_PERIPHERAL_ID_TC1    = ID_TC1,
  APV_PERIPHERAL_ID_TC2    = ID_TC2,
  APV_PERIPHERAL_ID_TC3    = ID_TC3,
  APV_PERIPHERAL_ID_TC4    = ID_TC4,
  APV_PERIPHERAL_ID_TC5    = ID_TC5,
  APV_PERIPHERAL_ID_TC6    = ID_TC6,
  APV_PERIPHERAL_ID_TC7    = ID_TC7,
  APV_PERIPHERAL_ID_TC8    = ID_TC8,
  APV_PERIPHERAL_ID_PWM    = ID_PWM,
  APV_PERIPHERAL_ID_ADC    = ID_ADC,
  APV_PERIPHERAL_ID_DACC   = ID_DACC,
  APV_PERIPHERAL_ID_DMAC   = ID_DMAC,
  APV_PERIPHERAL_ID_UOTGHS = ID_UOTGHS,
  APV_PERIPHERAL_ID_TRNG   = ID_TRNG,
  APV_PERIPHERAL_ID_EMAC   = ID_EMAC,
  APV_PERIPHERAL_ID_CAN0   = ID_CAN0,
  APV_PERIPHERAL_ID_CAN1   = ID_CAN1,
  APV_PERIPHERAL_IDS
  } apvPeripheralId_t;

typedef enum apvDigitalId_tTag
  {
  APV_DIGITAL_ID_IDENTITY_SELECT           = 0,
  APV_DIGITAL_ID_LSM9DS_CHIP_ENABLE_SELECT,
  APV_DIGITAL_RFM69HCW_CHIP_ENABLE_SELECT,
  APV_DIGITAL_NRF24L01_TX_CHIP_ENABLE_SELECT,
  APV_DIGITAL_NRF24L01_RX_CHIP_ENABLE_SELECT,
  APV_DIGITAL_HEARTBEAT_LED_SELECT,
  APV_DIGITAL_RFM69HCW_RESET_SELECT,
  APV_DIGITAL_IDS
  } apvDigitalId_t;

/******************************************************************************/

typedef enum apvSPIFixedChipSelects_tTag
  {
  APV_SPI_FIXED_CHIP_SELECT_0 = 0,
  APV_SPI_FIXED_CHIP_SELECT_1,
  APV_SPI_FIXED_CHIP_SELECT_2,
  APV_SPI_FIXED_CHIP_SELECT_3,
  APV_SPI_FIXED_CHIP_SELECTS
  } apvSPIFixedChipSelects_t;

/******************************************************************************/
/* SPI Peripheral Definitions :                                               */
/******************************************************************************/

typedef enum apvSPILastTransfer_tTag
  {
  APV_SPI_LAST_TRANSFER_NONE = 0,
  APV_SPI_LAST_TRANSFER_ACTIVE,
  APV_SPI_LAST_TRANSFER_SET
  } apvSPILastTransfer_t;

typedef enum apvPeripheralLineGroup_tTag
  {
  APV_PERIPHERAL_LINE_GROUP_A = 0,
  APV_PERIPHERAL_LINE_GROUP_B,
  APV_PERIPHERAL_LINE_GROUP_C,
  APV_PERIPHERAL_LINE_GROUP_D,
  APV_PERIPHERAL_LINE_GROUPS
  } apvPeripheralLineGroup_t;

/******************************************************************************/
/* Holding structure for the core timer-derived process timer set             */
/******************************************************************************/

typedef enum apvDurationTimerSource_tTag
  {
  APV_DURATION_TIMER_SOURCE_RTT = 0,
  APV_DURATION_TIMER_SOURCE_SYSTICK,
  APV_DURATION_TIMER_SOURCES
  } apvDurationTimerSource_t;

typedef enum apvDurationTimerType_tTag
  {
  APV_DURATION_TIMER_TYPE_NONE = 0,
  APV_DURATION_TIMER_TYPE_ONE_SHOT,
  APV_DURATION_TIMER_TYPE_PERIODIC,
  APV_DURATION_TIMER_TYPES
  } apvDurationTimerType_t;

typedef enum apvDurationTimerState_tTag
  {
  APV_DURATION_TIMER_STATE_STOPPED = 0,
  APV_DURATION_TIMER_STATE_RUNNING,
  APV_DURATION_TIMER_STATES
  } apvDurationTimerState_t;

typedef struct apvDurationTimer_tTag
  {
  uint32_t                 durationTimerIndex;                                  // 0 { durationTimer[index] } 31
  uint32_t                 durationTimerRequestedMicroSeconds;                  // lots of microseconds
  uint32_t                 durationTimerRequestedTicks;                         // the reload value for the down counter
  uint32_t                 durationTimerDownCounter;                            // just test for zero
  void                    (*durationTimerCallBack)(void *durationEventMessage); // called when the timer expires
  void                     *durationTimerMessage;                               // passed to the callback when the timer expires
  apvDurationTimerType_t   durationTimerType;                                   // a number of timer types can be supported
  apvDurationTimerState_t  durationTimerState;                                  // running or stopped
  } apvDurationTimer_t;

typedef struct apvCoreTimerBlock_tTag
  {
  uint32_t           timeBaseDivider;                               // the value loaded into RTT->RTT_MR:RTPRES from which all 
                                                                    // duration timers are derived
  apvDurationTimer_t durationTimer[APV_CORE_TIMER_DURATION_TIMERS]; // a fixed number of duration timers is allocated
  } apvCoreTimerBlock_t;

/******************************************************************************/
/* Lightweight FIFO Definitions :                                             */
/******************************************************************************/

typedef struct apvFIFOEntry_tTag
  {
  uint8_t  apvFIFOEntryKey;
  uint32_t apvFIFOEntryData;
  } apvFIFOEntry_t;

typedef struct apvFIFOStructure_tTag
  {
  apvFIFOEntry_t *apvFIFO;
  uint8_t         apvFIFOHeadPointer;
  uint8_t         apvFIFODepth;
  uint8_t         apvFIFOTailPointer;
  uint8_t         apvFIFOFillLevel;
  } apvFIFOStructure_t;

/******************************************************************************/

extern Pio                 ApvPeripheralLineControlBlock[APV_PERIPHERAL_LINE_GROUPS];   // shadow PIO control blocks
extern Pio                *ApvPeripheralLineControlBlock_p[APV_PERIPHERAL_LINE_GROUPS]; // physical block addresses
extern apvFIFOStructure_t  apvSpi0ChipSelectFIFO[APV_SPI_FIXED_CHIP_SELECTS];

/******************************************************************************/

extern APV_ERROR_CODE apvGetFIFOFillLevel(apvFIFOStructure_t *fifo,
                                          uint8_t            *fillLevel,
                                          bool                interruptControl);

/******************************************************************************/

#endif

/******************************************************************************/
/* (C) PulsingCoreSoftware Limited 2020 (C)                                   */
/******************************************************************************/
