/******************************************************************************/
/* (C) Pulsing Core Software 2018                                             */
/******************************************************************************/
/*                                                                            */
/* Mcp2210_Dev.h                                                              */
/* 11.12.18                                                                   */
/* Paul O'Brien                                                               */
/*                                                                            */
/* Ref : https://www.microchip.com/Developmenttools/ProductDetails/ADM00419#additional-summary */
/*                                                                            */
/******************************************************************************/

#ifndef _MCP2210_DEV_H_
#define _MCP2210_DEV_H_

/******************************************************************************/
/* Include Files :                                                            */
/******************************************************************************/

#include "mcp2210_dll_um.h"
#include "apv_RFM69HCW.h"
//#include "apv_nRF24L01.h"

/******************************************************************************/
/* Constants :                                                                */
/******************************************************************************/

#define MCP2210_DEFAULT_VID                            (0x04D8)
#define MCP2210_DEFAULT_PID                            (0x00DE)

#define MCP2210_DEVICE_INDEX_0                              (0)

#define MCP2210_OPEN_DEVICE_PATH_STRING_MAXIMUM_LENGTH   (1024)

#define MCP2210_DATA_DELAY_QUANTUM                        (100) // 100 microseconds

#define MCP2210_DEFAULT_CHIP_SELECT_MASK                    (0)

#define MCP2210_ADDRESS_BUFFER_LENGTH                       (8) // allows the address to be moved from array-offset[0] to array-offset[1] for SPI register-writes
#define nRF24L01_TX_DATA_BUFFER_LENGTH                     (32)

#define APV_NRF24L01_REGISTER_ADDRESS_NAME_LENGTH          (64)

// Output formatting definitions :
#define MCP2210_BREAK_MAXIMUM_LEADING_BLANK_LINES           (4)
#define MCP2210_BREAK_MAXIMUM_FOLLOWING_BLANK_LINES         (4)
#define MCP2210_BREAK_MAXIMUM_LINE_LENGTH                 (132)
#define MCP2210_BREAK_DEFAULT_CHARACTER                   ('*')

/******************************************************************************/
/* Type Definitions :                                                         */
/******************************************************************************/

// Internalise and enumerate the error codes
typedef enum Mcp2210DevErrorCodes_tTag
  {
  MCP2210_DEV_E_SUCCESS                       = E_SUCCESS                       ,
  MCP2210_DEV_E_ERR_UNKNOWN_ERROR             = E_ERR_UNKOWN_ERROR              ,
  MCP2210_DEV_E_ERR_INVALID_PARAMETER         = E_ERR_INVALID_PARAMETER         ,
  MCP2210_DEV_E_ERR_BUFFER_TOO_SMALL          = E_ERR_BUFFER_TOO_SMALL          ,
  MCP2210_DEV_E_ERR_NULL                      = E_ERR_NULL                      ,
  MCP2210_DEV_E_ERR_MALLOC                    = E_ERR_MALLOC                    ,
  MCP2210_DEV_E_ERR_INVALID_HANDLE_VALUE      = E_ERR_INVALID_HANDLE_VALUE      ,
  MCP2210_DEV_E_ERR_FIND_DEV                  = E_ERR_FIND_DEV                  ,
  MCP2210_DEV_E_ERR_NO_SUCH_INDEX             = E_ERR_NO_SUCH_INDEX             ,
  MCP2210_DEV_E_ERR_DEVICE_NOT_FOUND          = E_ERR_DEVICE_NOT_FOUND          ,
  MCP2210_DEV_E_ERR_INTERNAL_BUFFER_TOO_SMALL = E_ERR_INTERNAL_BUFFER_TOO_SMALL ,
  MCP2210_DEV_E_ERR_OPEN_DEVICE_ERROR         = E_ERR_OPEN_DEVICE_ERROR         ,
  MCP2210_DEV_E_ERR_CONNECTION_ALREADY_OPENED = E_ERR_CONNECTION_ALREADY_OPENED ,
  MCP2210_DEV_E_ERR_CLOSE_FAILED              = E_ERR_CLOSE_FAILED              ,
  MCP2210_DEV_E_ERR_NO_SUCH_SERIALNR          = E_ERR_NO_SUCH_SERIALNR          ,
  MCP2210_DEV_E_ERR_HID_RW_TIMEOUT            = E_ERR_HID_RW_TIMEOUT            ,
  MCP2210_DEV_E_ERR_HID_RW_FILEIO             = E_ERR_HID_RW_FILEIO             ,
  MCP2210_DEV_E_ERR_CMD_FAILED                = E_ERR_CMD_FAILED                ,
  MCP2210_DEV_E_ERR_CMD_ECHO                  = E_ERR_CMD_ECHO                  ,
  MCP2210_DEV_E_ERR_SUBCMD_ECHO               = E_ERR_SUBCMD_ECHO               ,
  MCP2210_DEV_E_ERR_SPI_EXTERN_MASTER         = E_ERR_SPI_EXTERN_MASTER         ,
  MCP2210_DEV_E_ERR_SPI_TIMEOUT               = E_ERR_SPI_TIMEOUT               ,
  MCP2210_DEV_E_ERR_SPI_RX_INCOMPLETE         = E_ERR_SPI_RX_INCOMPLETE         ,
  MCP2210_DEV_E_ERR_SPI_XFER_ONGOING          = E_ERR_SPI_XFER_ONGOING          ,
  MCP2210_DEV_E_ERR_BLOCKED_ACCESS            = E_ERR_BLOCKED_ACCESS            ,
  MCP2210_DEV_E_ERR_EEPROM_WRITE_FAIL         = E_ERR_EEPROM_WRITE_FAIL         ,
  MCP2210_DEV_E_ERR_WRONG_PASSWD              = E_ERR_WRONG_PASSWD              ,
  MCP2210_DEV_E_ERR_ACCESS_DENIED             = E_ERR_ACCESS_DENIED             ,
  MCP2210_DEV_E_ERR_NVRAM_PROTECTED           = E_ERR_NVRAM_PROTECTED           ,
  MCP2210_DEV_E_ERR_PASSWD_CHANGE             = E_ERR_PASSWD_CHANGE             ,
  MCP2210_DEV_E_ERR_STRING_DESCRIPTOR         = E_ERR_STRING_DESCRIPTOR         ,
  MCP2210_DEV_E_ERR_STRING_TOO_LARGE          = E_ERR_STRING_TOO_LARGE          ,
  MCP2210_DEV_NO_ERROR                        = MCP2210_DEV_E_SUCCESS,
  MCP2210_DEV_ERROR_CODES
  } Mcp2210DevErrorCodes_t;

typedef struct Mcp2210GpioConfiguration_tTag 
  {
  unsigned char pinFunctions[MCP2210_GPIO_NR];
  unsigned int  pinOutputValues[MCP2210_GPIO_NR];
  unsigned int  pinDirections[MCP2210_GPIO_NR];
  unsigned char remoteWakeupMode[MCP2210_GPIO_NR];
  unsigned char interruptPulseCountMode[MCP2210_GPIO_NR];
  unsigned char spiBusReleaseMode[MCP2210_GPIO_NR];
  } Mcp2210GpioConfiguration_t;

typedef enum Mcp2210GpioPinNumber_tTag
  {
  MCP2210_GPIO_PIN_0 = 0,
  MCP2210_GPIO_PIN_1,
  MCP2210_GPIO_PIN_2,
  MCP2210_GPIO_PIN_3,
  MCP2210_GPIO_PIN_4,
  MCP2210_GPIO_PIN_5,
  MCP2210_GPIO_PIN_6,
  MCP2210_GPIO_PIN_7,
  MCP2210_GPIO_PIN_8,
  MCP2210_GPIO_PINS
  } Mcp2210GpioPinNumber_t;

typedef enum Mcp2210GpioPinDirection_tTag
  {
  MCP2210_PIN_DIR_OUT = 0,
  MCP2210_PIN_DIR_IN,
  MCP2210_PIN_DIRS
  } Mcp2210GpioPinDirection_t;

typedef enum Mcp2210GpioPinLevel_tTag
  {
  MCP2210_PIN_LEVEL_LOW,
  MCP2210_PIN_LEVEL_HIGH,
  MCP2210_PIN_LEVELS
  } Mcp2210GpioPinLevel_t;

typedef struct Mcp2210DevSpiConfiguration_tTag
  {
  unsigned int  transferSpeed;
  unsigned int  chipSelectIdleLevel;
  unsigned int  chipSelectActiveLevel;
  unsigned int  csToFirstDataDelay;
  unsigned int  lastDataToCsDelay;
  unsigned int  intraDataToDataDelay;
  unsigned int  bytesPerTransfer;
  unsigned char spiMode;
  } Mcp2210DevSpiConfiguration_t;

typedef enum Mcp2210DevSpiChipSelects_tTag  
  {
  MCP2210_CHIP_SELECT_0 = 0,
  MCP2210_CHIP_SELECT_1,
  MCP2210_CHIP_SELECT_2,
  MCP2210_CHIP_SELECT_3,
  MCP2210_CHIP_SELECT_4,
  MCP2210_CHIP_SELECT_5,
  MCP2210_CHIP_SELECT_6,
  MCP2210_CHIP_SELECT_7,
  MCP2210_CHIP_SELECT_8,
  MCP2210_CHIP_SELECTS
  } Mcp2210DevSpiChipSelects_t;

typedef enum Mcp2210DevSpiModes_tTag
  {
  MCP2210_SPI_MODE_0 = 0,
  MCP2210_SPI_MODE_1,
  MCP2210_SPI_MODE_2,
  MCP2210_SPI_MODE_3,
  MCP2210_SPI_MODES
  } Mcp2210DevSpiModes_t;

/******************************************************************************/
/* Global Variable Declarations :                                             */
/******************************************************************************/

extern wchar_t                      Mcp2210DllLibraryVersion[MPC2210_LIBRARY_VERSION_SIZE];
                                    
extern wchar_t                      Mcp2210OpenDevicePath[MCP2210_OPEN_DEVICE_PATH_STRING_MAXIMUM_LENGTH];
                                    
extern Mcp2210GpioConfiguration_t   Mcp2210GpioNvRamConfigurationWrite,
                                    Mcp2210GpioVmConfigurationWrite,
                                    Mcp2210GpioNvRamConfigurationRead,
                                    Mcp2210GpioVmConfigurationRead;

extern Mcp2210DevSpiConfiguration_t Mcp2210SpiVmConfigurationRead,
                                    Mcp2210SpiVmConfigurationWrite;

extern unsigned char                nRF24L01TxDataBuffer[nRF24L01_TX_DATA_BUFFER_LENGTH];

extern APV_TYPE_UCHAR               rfm69hcwRxDataBuffer[APV_RFM69HCW_REGISTER_SETUP_MAXIMUM_WIDTH],
                                    rfm69hcwTxDataBuffer[APV_RFM69HCW_REGISTER_SETUP_MAXIMUM_WIDTH],
                                    rfm69hcwRegisterCommand[APV_RFM69HCW_REGISTER_SETUP_MAXIMUM_WIDTH];
//                                    registerCommandReceive[APV_nRF24L01_REGISTER_COMMAND_BUFFER_LENGTH];

//extern const unsigned char          nRF24L01RegisterNames[APV_NRF24L01_REGISTER_ADDRESS_SET][APV_NRF24L01_REGISTER_ADDRESS_NAME_LENGTH];

/******************************************************************************/
/* Function Declarations :                                                    */
/******************************************************************************/

extern Mcp2210DevErrorCodes_t Mcp2210DevInitialise(wchar_t  *libraryVersionString,
                                                   int      *libraryVersionStringLength,
                                                   int      *numberOFConnectedDevices);
extern Mcp2210DevErrorCodes_t Mcp2210DeviceOpen(void       **deviceHandle_p,
                                                wchar_t        *devicePath,
                                                unsigned long  *devicePathStringLength);
extern Mcp2210DevErrorCodes_t Mcp2210DevReadGpioConfig(void          **deviceHandle_p,
                                                       unsigned char   chipSettingMode,
                                                       unsigned char  *pinFunctions,
                                                       unsigned int   *pinOutputValues,
                                                       unsigned int   *pinDirections,
                                                       unsigned char  *remoteWakeupMode,
                                                       unsigned char  *interruptPulseCountMode,
                                                       unsigned char  *spiBusReleaseMode);
extern Mcp2210DevErrorCodes_t Mcp2210DevWriteGpioConfig(void          **deviceHandle_p,
                                                        unsigned char   chipSettingMode,
                                                        unsigned char  *pinFunctions,
                                                        unsigned int    pinOutputValues,
                                                        unsigned int    pinDirections,
                                                        unsigned char   remoteWakeupMode,
                                                        unsigned char   interruptPulseMode,
                                                        unsigned char   spiBusReleaseMode);
extern APV_ERROR_CODE         apvMcp2210Rfm69HcwAtomicReset(void **deviceHandle_p);
extern Mcp2210DevErrorCodes_t Mcp2210SetGpioPinDirection(void                      **deviceHandle_p,
                                                         Mcp2210GpioPinNumber_t      pinNumber,
                                                         Mcp2210GpioPinDirection_t   pinDirection);
extern Mcp2210DevErrorCodes_t Mcp2210GetGpioPinDirection(void                      **deviceHandle_p,
                                                         Mcp2210GpioPinNumber_t      pinNumber,
                                                         Mcp2210GpioPinDirection_t  *pinDirection);
extern Mcp2210DevErrorCodes_t Mcp2210SetGpioPinLevel(void                   **deviceHandle_p,
                                                     Mcp2210GpioPinNumber_t   pinNumber,
                                                     Mcp2210GpioPinLevel_t    pinLevel);
extern Mcp2210DevErrorCodes_t Mcp2210GetGpioPinLevel(void                   **deviceHandle_p,
                                                     Mcp2210GpioPinNumber_t   pinNumber,
                                                     Mcp2210GpioPinLevel_t   *pinLevel);
extern Mcp2210DevErrorCodes_t Mcp2210DevReadSpiConfig(void          **deviceHandle_p,
                                                      unsigned char   chipSettingMode,
                                                      unsigned int   *transferSpeed,
                                                      unsigned int   *chipSelectIdleLevel,
                                                      unsigned int   *chipSelectActiveLevel,
                                                      unsigned int   *csToFirstDataDelay,
                                                      unsigned int   *lastDataToCsDelay,
                                                      unsigned int   *intraDataToDataDelay,
                                                      unsigned int   *bytesPerTransfer,
                                                      unsigned char  *spiMode);
extern Mcp2210DevErrorCodes_t Mcp2210DevWriteSpiConfig(void          **deviceHandle_p,
                                                       unsigned char   chipSettingMode,
                                                       unsigned int    transferSpeed,
                                                       unsigned int    chipSelectIdleLevel,
                                                       unsigned int    chipSelectActiveLevel,
                                                       unsigned int    csToFirstDataDelay,
                                                       unsigned int    lastDataToCsDelay,
                                                       unsigned int    intraDataToDataDelay,
                                                       unsigned int    bytesPerTransfer,
                                                       unsigned char   spiMode);
extern Mcp2210DevErrorCodes_t Mcp2210DevWriteRfm69HcwRegister(      apv_rfm69hcwRegisterAddressSet_t  rfm69HcwRegister,
                                                              const apv_rfm69hcwRegisterDefinition_t *rfm69hcwRegisterDefinitions,
                                                                    APV_TYPE_UCHAR                   *rfm69hcwTxRegisterCharacters,
                                                                    APV_TYPE_UCHAR                   *rfm69hcwRxRegisterCharacters,
                                                                    APV_TYPE_UINT                    *rfm69hcwRegisterCharactersLength,
                                                                    APV_TYPE_VOID                    *mcp2210DeviceHandle,
                                                                    Mcp2210DevSpiConfiguration_t     *mcp2210SpiConfiguration);
extern Mcp2210DevErrorCodes_t Mcp2210DevReadRfm69HcwRegister(      apv_rfm69hcwRegisterAddressSet_t  rfm69HcwRegister,
                                                             const apv_rfm69hcwRegisterDefinition_t *rfm69hcwRegisterDefinitions,
                                                                   APV_TYPE_UCHAR                   *rfm69hcwRegisterCharacters,
                                                                   APV_TYPE_UINT                    *rfm69hcwRegisterCharactersLength,
                                                                   APV_TYPE_VOID                    *mcp2210DeviceHandle,
                                                                   Mcp2210DevSpiConfiguration_t     *mcp2210SpiConfiguration);
/*extern Mcp2210DevErrorCodes_t Mcp2210DevReadnRF24L01Register(      apv_nRF24LO1RegisterAddressSet_t  nRF24L01Register,
                                                             const apv_nRF24L01RegisterDefinition_t *nRF24L01RegisterDefinitions,
                                                             unsigned char                          *nRF24L01RegisterCharacters,
                                                             unsigned int                           *nRF24L01RegisterCharactersLength,
                                                             unsigned char                          *nRF24L01StatusCharacter,
                                                             void                                   *mcp2210DeviceHandle,
                                                             Mcp2210DevSpiConfiguration_t           *mcp2210SpiConfiguration);
extern Mcp2210DevErrorCodes_t Mcp2210DevWritenRF24L01Register(      apv_nRF24LO1RegisterAddressSet_t  nRF24L01Register,
                                                              const apv_nRF24L01RegisterDefinition_t *nRF24L01RegisterDefinitions,
                                                                    unsigned char                    *nRF24L01RegisterPayload,
                                                                    unsigned int                      nRF24L01RegisterPayloadLength,
                                                                    unsigned char                    *nRF24L01StatusCharacter,
                                                                    void                             *mcp2210DeviceHandle,
                                                                    Mcp2210DevSpiConfiguration_t     *mcp2210SpiConfiguration);
extern Mcp2210DevErrorCodes_t Mcp2210DevChipEnableControl(void                          **deviceHandle_p,
                                                          apv_nRF24L01ChipEnableState_t   chipEnableState,
                                                          Mcp2210GpioPinNumber_t          chipEnablePinNumber); */

/******************************************************************************/
/* Register READ Decoders :                                                   */
/******************************************************************************/

extern void                   Mcp2210DecodeRegisterConfig(unsigned char *registerConfig);
extern void                   Mcp2210DecodeRegisterEnableAutoAck(unsigned char *registerEnAA);
extern void                   Mcp2210DecodeRegisterEnableRxPipe(unsigned char *registerEnRxAddr);
extern void                   Mcp2210DecodeRegisterSetupAddressWidths(unsigned char *registerAddressWidths);
extern void                   Mcp2210DecodeRegisterSetupAutomaticRetransmission(unsigned char *registerAutoReTx);
extern void                   Mcp2210DecodeRegisterRfChannel(unsigned char *registerRfChannel);
extern void                   Mcp2210DecodeRegisterRfSetup(unsigned char *registerRfSetup);
extern void                   Mcp2210DecodeRegisterStatus(unsigned char *registerStatus);
extern void                   Mcp2210DecodeRegisterObserveTx(unsigned char *registerObserveTx);
extern void                   Mcp2210DecodeRegisterRxPowerDetector(unsigned char *registerRxPower);

/******************************************************************************/
/* Register WRITE Encoders :                                                  */
/******************************************************************************/

/* extern Mcp2210DevErrorCodes_t Mcp2210DevWriteTxAddress(      unsigned char                     *radioTxAddress,
                                                             unsigned int                       radioAddressLength,
                                                             apv_nRF24L01RadioStateRegisters_t *radioVolatileState,
                                                       const apv_nRF24L01RegisterDefinition_t  *apv_nRF24L01RegisterDefinitions,
                                                             unsigned char                     *nRF24L01StatusCharacter,
                                                             void                              *mcp2210DeviceHandle,
                                                             Mcp2210DevSpiConfiguration_t      *mcp2210SpiConfiguration); */
/* extern Mcp2210DevErrorCodes_t Mcp2210DevWriteRxPipelineAddress(      unsigned char                     *radioRxAddress,
                                                                     apv_nRF24L01DataPipe_t             radioRxDataPipe,
                                                                     unsigned int                       radioAddressLength,
                                                                     apv_nRF24L01RadioStateRegisters_t *radioVolatileState,
                                                               const apv_nRF24L01RegisterDefinition_t  *apv_nRF24L01RegisterDefinitions,
                                                                     unsigned char                     *nRF24L01StatusCharacter,
                                                                     void                              *mcp2210DeviceHandle,
                                                                     Mcp2210DevSpiConfiguration_t      *mcp2210SpiConfiguration);
extern Mcp2210DevErrorCodes_t Mcp2210DevWriteRxPipeWidth(      apv_nRF24L01DataPipe_t             radioRxDataPipe,
                                                               unsigned int                       radioRxDataPipePayloadWidth,
                                                               apv_nRF24L01RadioStateRegisters_t *radioVolatileState,
                                                         const apv_nRF24L01RegisterDefinition_t  *apv_nRF24L01RegisterDefinitions,
                                                               unsigned char                     *nRF24L01StatusCharacter,
                                                               void                              *mcp2210DeviceHandle,
                                                               Mcp2210DevSpiConfiguration_t      *mcp2210SpiConfiguration); */

extern Mcp2210DevErrorCodes_t Mcp2210DevFlushTxFifo(unsigned char                *nRF24L01StatusCharacter,
                                                    void                         *mcp2210DeviceHandle,
                                                    Mcp2210DevSpiConfiguration_t *mcp2210SpiConfiguration);

extern Mcp2210DevErrorCodes_t Mcp2210DevWriteRegisterPayload(unsigned char                *nRF24L01RegisterPayload,
                                                             unsigned int                  nRF24L01RegisterPayloadLength,
                                                             unsigned char                *nRF24L01StatusCharacter,
                                                             void                         *mcp2210DeviceHandle,
                                                             Mcp2210DevSpiConfiguration_t *mcp2210SpiConfiguration);

/* extern Mcp2210DevErrorCodes_t Mcp2210EncodeRegisterRfSetup(      unsigned char                 *rfSetup,
                                                                 apv_nRF24L01RfSetupPower_t     rfPower,
                                                                 bool                           rfDataRateLow,
                                                                 apv_nRF24L01RfSetupDataRate_t  rfDataRate,
                                                                 bool                           rfContinuousWave,
                                                                 apv_nRF24L01RadioStateRegisters_t *radioVolatileState,
                                                           const apv_nRF24L01RegisterDefinition_t  *apv_nRF24L01RegisterDefinitions);
extern Mcp2210DevErrorCodes_t Mcp2210RxTxControl(       unsigned char                     *configRegister,
                                                        apv_nRF24L01DuplexId_t             txRxSelection,
                                                        apv_nRF24L01RadioStateRegisters_t *radioVolatileState,
                                                  const apv_nRF24L01RegisterDefinition_t  *apv_nRF24L01RegisterDefinitions);
extern Mcp2210DevErrorCodes_t Mcp2210PowerControl(      unsigned char                          *configRegister,
                                                        apv_nRF24L01PowerState_t                powerState,
                                                        apv_nRF24L01RadioStateRegisters_t      *radioVolatileState,
                                                  const apv_nRF24L01RegisterDefinition_t *apv_nRF24L01RegisterDefinitions);
extern Mcp2210DevErrorCodes_t Mcp2210EncodeRegisterSetupRetr(      unsigned char                     *retransmissionRegister,
                                                                   unsigned char                      retransmissions,
                                                                   unsigned char                      retransmissionDelay,
                                                                   apv_nRF24L01RadioStateRegisters_t *radioVolatileState,
                                                             const apv_nRF24L01RegisterDefinition_t  *apv_nRF24L01RegisterDefinition); */

/******************************************************************************/
/* Discrete nRf24L01 read commands :                                          */
/******************************************************************************/

extern Mcp2210DevErrorCodes_t Mcp2210DevFlushRxFifo(unsigned char                *nRF24L01StatusCharacter,
                                                    void                         *mcp2210DeviceHandle,
                                                    Mcp2210DevSpiConfiguration_t *mcp2210SpiConfiguration);
extern Mcp2210DevErrorCodes_t Mcp2210DevReadTopRxPayloadWidth(unsigned int                 *nRF24L01RegisterPayloadWidth,
                                                              unsigned char                *nRF24L01StatusCharacter,
                                                              void                         *mcp2210DeviceHandle,
                                                              Mcp2210DevSpiConfiguration_t *mcp2210SpiConfiguration);

extern Mcp2210DevErrorCodes_t Mcp2210DevReadRegisterPayload(unsigned char                    *nRF24L01RegisterPayload,
                                                            unsigned int                      nRF24L01RegisterPayloadLength,
                                                            unsigned char                    *nRF24L01StatusCharacter,
                                                            void                             *mcp2210DeviceHandle,
                                                            Mcp2210DevSpiConfiguration_t     *mcp2210SpiConfiguration);

/******************************************************************************/
/* Register-map based nRF24L01 read commands :                                */
/******************************************************************************/

extern void                   Mcp2210ReadDataPipeReceiveAddress(unsigned char *registerReceiveAddress,
                                                                unsigned int   receiveAddressLength,
                                                                unsigned int   receiveDataPipeNumber);
extern void                   Mcp2210ReadTransmitAddress(unsigned char *registerTransmitAddress,
                                                         unsigned int   transmitAddressLength);
extern void                   Mcp2210ReadDataPipeReceivePayloadLength(unsigned char *registerPayloadLength,
                                                                      unsigned int   receiveDataPipeNumber);
extern void                   Mcp2210ReadFifoStatus(unsigned char *fifoStatus);
extern void                   Mcp2210ReadDynamicPayloadState(unsigned char *dynamicPayloadState);
extern void                   Mcp2210ReadAddressFeatures(unsigned char *features);

extern void                   Mcp2210PrintGpioConfiguration(char                       *gpioConfigurationLabel,
                                                            Mcp2210GpioConfiguration_t *gpioConfiguration);
extern Mcp2210DevErrorCodes_t mcp2210ReadRfw69HcwTemperature(APV_TYPE_VOID  *mcp2210DeviceHandle,
                                                             APV_TYPE_UINT8 *radioTemperature);
extern void                   Mcp2210DevPrintSpiConfiguration(char                         *spiConfigurationLabel,
                                                              Mcp2210DevSpiConfiguration_t *spiConfiguration);
extern void                   Mcp2210PrintBreak(size_t leadingBlankLines,
                                                size_t followingBlankLines,
                                                size_t breakLineLength,
                                                char   breakCharacter);

/******************************************************************************/

#endif

/******************************************************************************/
/* (C) Pulsing Core Software 2018                                             */
/******************************************************************************/