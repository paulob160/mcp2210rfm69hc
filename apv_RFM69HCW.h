/******************************************************************************/
/* (C) PulsingCoreSoftware Limited 2020 (C)                                   */
/******************************************************************************/
/*                                                                            */
/* apv_RFM69HCW.h                                                             */
/* 11.06.20                                                                   */
/* Paul O'Brien                                                               */
/*                                                                            */
/* - control and status for the Semtech RFM69HCW 868 - 915MHz wireless.       */
/*                                                                            */
/*   Ref: SEMTECH, "SX1231 Transceiver Low Power Integrated UHF Transceiver", */
/*        Rev. 7, June 2013                                                   */
/*                                                                            */
/******************************************************************************/

#ifndef _APV_RFM69HCW_H_
#define _APV_RFM69HCW_H_

/******************************************************************************/

#include "ApvTypes.h"
#include "ApvError.h"
#include "rfm69hcwRegisters.h"
#include "apv_RFM69HCW_Misc.h"

/******************************************************************************/

#define APV_RFM69HCW_MANUAL_RESET_COUNTDOWN        (10) // 10ms derived from the main loop
                                                   
#define APV_RFM69HCW_NULL_CHARACTER                ((APV_RFM69HCW_FIELD_SIZE)0x00) // the low-byte of a register read is a dummy
                                                   
#define APV_RFM69HCW_RESET_ASSERTION_DELAY_PERIOD  ((APV_TYPE_UINT64)(150000))  // 150 microseconds as nanoseconds
#define APV_RFM69HCW_RESET_DWELL_DELAY_PERIOD      ((APV_TYPE_UINT64)(7500000)) // 7.5 milliseconds as nanoseconds
                                                   
#define APV_RFM69HCW_VERSION_NUMBER                ((APV_TYPE_UINT8)0x24) // radio version number as of 28.06.20
#define APV_RFM69HCW_VERSION_MASK                  ((APV_TYPE_UINT8)0xFF)

#define APV_RFM69HCW_REGISTER_DEFAULT_READ_MASK    (0xFF)
#define APV_RFM69HCW_REGISTER_DEFAULT_WRITE_MASK   (0xFF)

#define APV_RFM69HCW_REGISTER_SETUP_MINIMUM_WIDTH   (2) // most register SPI transactions are one address-byte + 
                                                        // one data-byte
#define APV_RFM69HCW_REGISTER_SETUP_MAXIMUM_WIDTH  (66) // payload FIFO lengths may be up to 66-bytes
                                                  
#define APV_RFM69HCW_REGISTER_TRANSFER_SPEED       (250000) // SPI transaction clock speed
                                                  
#define APV_RFM69HCW_COMMAND_NOP                   ((APV_TYPE_UINT8)0x00)
                                                  
#define APV_RFM69HCW_REGISTER_COMMAND_OFFSET       (0)    // register read/write transactions require the address-
                                                          // byte in the first character
#define APV_RFM69HCW_REGISTER_DATA_OFFSET          (1)    // commands are <address> | [ <read> | <write> ] + 0 { <data-bytes> } 1
#define APV_RFM69HCW_REGISTER_DATA_BUFFER_OFFSET   (0)    // register DATA is passed around in a buffer at offset 0

#define APV_RFM69HCW_CHIP_RESET_MASK               (0x01) // the RESET pin is currently assigned to MCP2210 pin #0
#define APV_RFM69HCW_CHIP_RESET_PRE_LOW            (1)    // keep RESET low for >150us
#define APV_RFM69HCW_CHIP_RESET_HIGH               (1)    // keep RESET high for > 100us
#define APV_RFM69HCW_CHIP_RESET_POST_LOW          (10)    // post-RESET wait for >5ms

#define APV_RFM69HCW_REGISTER_NAME_MAXIMUM_LENGTH (32)

#define APV_RFM69HCW_REGISTER_VERSION           (0x24)    // the current chip version is 0x24

/******************************************************************************/

typedef APV_TYPE_UINT8 APV_RFM69HCW_FIELD_SIZE;

// This enumeration is a direct mapping for the RFM69HCW register set
typedef enum apv_rfm69hcwRegisterAddressSet_tTag
  {
  APV_RFM69HCW_REG_FIFO = REG_FIFO,          
  APV_RFM69HCW_REG_OPMODE,        
  APV_RFM69HCW_REG_DATAMODUL,     
  APV_RFM69HCW_REG_BITRATEMSB,    
  APV_RFM69HCW_REG_BITRATELSB,    
  APV_RFM69HCW_REG_FDEVMSB,       
  APV_RFM69HCW_REG_FDEVLSB,       
  APV_RFM69HCW_REG_FRFMSB,        
  APV_RFM69HCW_REG_FRFMID,        
  APV_RFM69HCW_REG_FRFLSB,        
  APV_RFM69HCW_REG_OSC1,          
  APV_RFM69HCW_REG_AFCCTRL,       
  APV_RFM69HCW_REG_LOWBAT,        
  APV_RFM69HCW_REG_LISTEN1,       
  APV_RFM69HCW_REG_LISTEN2,       
  APV_RFM69HCW_REG_LISTEN3,       
  APV_RFM69HCW_REG_VERSION,       
  APV_RFM69HCW_REG_PALEVEL,       
  APV_RFM69HCW_REG_PARAMP,        
  APV_RFM69HCW_REG_OCP,           
  APV_RFM69HCW_REG_AGCREF,         // not present on RFM69/SX1231
  APV_RFM69HCW_REG_AGCTHRESH1,     // not present on RFM69/SX1231
  APV_RFM69HCW_REG_AGCTHRESH2,     // not present on RFM69/SX1231
  APV_RFM69HCW_REG_AGCTHRESH3,     // not present on RFM69/SX1231
  APV_RFM69HCW_REG_LNA,           
  APV_RFM69HCW_REG_RXBW,          
  APV_RFM69HCW_REG_AFCBW,         
  APV_RFM69HCW_REG_OOKPEAK,       
  APV_RFM69HCW_REG_OOKAVG,        
  APV_RFM69HCW_REG_OOKFIX,        
  APV_RFM69HCW_REG_AFCFEI,        
  APV_RFM69HCW_REG_AFCMSB,        
  APV_RFM69HCW_REG_AFCLSB,        
  APV_RFM69HCW_REG_FEIMSB,        
  APV_RFM69HCW_REG_FEILSB,        
  APV_RFM69HCW_REG_RSSICONFIG,    
  APV_RFM69HCW_REG_RSSIVALUE,     
  APV_RFM69HCW_REG_DIOMAPPING1,   
  APV_RFM69HCW_REG_DIOMAPPING2,   
  APV_RFM69HCW_REG_IRQFLAGS1,     
  APV_RFM69HCW_REG_IRQFLAGS2,     
  APV_RFM69HCW_REG_RSSITHRESH,    
  APV_RFM69HCW_REG_RXTIMEOUT1,    
  APV_RFM69HCW_REG_RXTIMEOUT2,    
  APV_RFM69HCW_REG_PREAMBLEMSB,   
  APV_RFM69HCW_REG_PREAMBLELSB,   
  APV_RFM69HCW_REG_SYNCCONFIG,    
  APV_RFM69HCW_REG_SYNCVALUE1,    
  APV_RFM69HCW_REG_SYNCVALUE2,    
  APV_RFM69HCW_REG_SYNCVALUE3,    
  APV_RFM69HCW_REG_SYNCVALUE4,    
  APV_RFM69HCW_REG_SYNCVALUE5,    
  APV_RFM69HCW_REG_SYNCVALUE6,    
  APV_RFM69HCW_REG_SYNCVALUE7,    
  APV_RFM69HCW_REG_SYNCVALUE8,    
  APV_RFM69HCW_REG_PACKETCONFIG1, 
  APV_RFM69HCW_REG_PAYLOADLENGTH, 
  APV_RFM69HCW_REG_NODEADRS,      
  APV_RFM69HCW_REG_BROADCASTADRS, 
  APV_RFM69HCW_REG_AUTOMODES,     
  APV_RFM69HCW_REG_FIFOTHRESH,    
  APV_RFM69HCW_REG_PACKETCONFIG2, 
  APV_RFM69HCW_REG_AESKEY1,       
  APV_RFM69HCW_REG_AESKEY2,       
  APV_RFM69HCW_REG_AESKEY3,       
  APV_RFM69HCW_REG_AESKEY4,       
  APV_RFM69HCW_REG_AESKEY5,       
  APV_RFM69HCW_REG_AESKEY6,       
  APV_RFM69HCW_REG_AESKEY7,       
  APV_RFM69HCW_REG_AESKEY8,       
  APV_RFM69HCW_REG_AESKEY9,       
  APV_RFM69HCW_REG_AESKEY10,      
  APV_RFM69HCW_REG_AESKEY11,      
  APV_RFM69HCW_REG_AESKEY12,      
  APV_RFM69HCW_REG_AESKEY13,      
  APV_RFM69HCW_REG_AESKEY14,      
  APV_RFM69HCW_REG_AESKEY15,      
  APV_RFM69HCW_REG_AESKEY16,      
  APV_RFM69HCW_REG_TEMP1,         
  APV_RFM69HCW_REG_TEMP2,
  APV_RFM69HCW_REGISTER_ADDRESS_SET
  } apv_rfm69hcwRegisterAddressSet_t;

// The fixed part of the register definitions
typedef struct apv_rfm69hcwRegisterDefinition_tTag
  {
  apv_rfm69hcwRegisterAddressSet_t apv_rfm69hcwRegisterAddress;
  APV_TYPE_UINT8                   apv_rfm69hcwRegisterDefault;
  APV_TYPE_UINT8                   apv_rfm69hcwRegisterReset;
  APV_TYPE_UINT8                   spv_rfm69hcwRegisterWriteMask;
  APV_TYPE_UINT8                   spv_rfm69hcwRegisterReadMask;  
  APV_TYPE_BOOLEAN                 apv_rfm69hcwRegisterInUse;
  } apv_rfm69hcwRegisterDefinition_t;

// The variable part of the register definitions
typedef struct apv_rfm69hcwRegisters_tTag
  {
  const apv_rfm69hcwRegisterDefinition_t *apv_rfm69hcwRegisterDefinition;
  APV_TYPE_UINT8                          apv_rfm69hcwRegister; // the register "shadow"
  } apv_rfm69hcwRegisters_t;

// Register read/write flag (address bit #7) : 
typedef enum apv_rfm69hcwRegisterReadWrite_tTag
  {
  APV_RFM69HCW_REGISTER_READ_DATA      = 0x00,
  APV_RFM69HCW_REGISTER_WRITE_DATA     = 0x80,
  APV_RFM69HCW_REGISTER_STREAM_DATA    = 0x7f,
  APV_RFM69HCW_REGISTER_NO_ACTION      = 0xff,
  APV_RFM69HCW_REGISTER_READ_WRITE_SET = 4
  } apv_rfm69hcwRegisterReadWrite_t;

typedef enum apv_rfm69hcwChipResetLevel_tTag
  {
  APV_RFM69HCW_CHIP_RESET_LEVEL_LOW  = 0,
  APV_RFM69HCW_CHIP_RESET_LEVEL_HIGH = 1,
  APV_RFM69HCW_CHIP_RESETS
  } apv_rfm69hcwChipResetLevel_t
  ;
/******************************************************************************/

extern APV_ERROR_CODE apvInitialiseRfm69Hcw(apvCoreTimerBlock_t      *apvCoreTimerBlock,
                                            apvSPIFixedChipSelects_t  spiChipSelect);
extern APV_ERROR_CODE apvRfm69HcwWriteRegisterDefault(apv_rfm69hcwRegisterAddressSet_t apv_rfm69HcwRegisterNumber);
extern APV_ERROR_CODE apvRfm69HcwAccessShallowRegister(Spi                              *spiControlBlock_p,
                                                       apvSPIFixedChipSelects_t          spiChipSelect,
                                                       apv_rfm69hcwRegisterReadWrite_t   spiReadWriteDirection,
                                                       apv_rfm69hcwRegisterAddressSet_t  rfm69hcwRegisterAddress,
                                                       APV_TYPE_UINT8                   *rfm69hcwRegister);
extern APV_TYPE_VOID    apvRfm69HcwStateTimer(APV_TYPE_VOID *durationEventMessage);
extern APV_ERROR_CODE   apvRfm69HcwAtomicReset(apvCoreTimerBlock_t *apvCoreTimeBaseBlock);
extern APV_ERROR_CODE   apvTriggerRfm69HcwManualReset(APV_TYPE_VOID);
extern APV_TYPE_BOOLEAN apvTerminateRfm69HcwManualReset(APV_ERROR_CODE *terminationError);

/******************************************************************************/

extern const apv_rfm69hcwRegisterDefinition_t apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REGISTER_ADDRESS_SET];
extern       apv_rfm69hcwRegisters_t          apv_rfm69hcwRegisters[APV_RFM69HCW_REGISTER_ADDRESS_SET];
extern APV_TYPE_CHAR                          apv_rfm69hcwRegisterNames[APV_RFM69HCW_REGISTER_ADDRESS_SET][APV_RFM69HCW_REGISTER_NAME_MAXIMUM_LENGTH];

extern       apv_rfm69hcwRegisterReadWrite_t  apvRfm69HcwPrecedingAccess;
extern       APV_TYPE_BOOLEAN                 apvRfm69HcwTimerFlag;
extern       APV_TYPE_BOOLEAN                 apvRfm69HcwTimerExpiredFlag;
extern       APV_TYPE_BOOLEAN                 apvRfm69HcwManualResetFlag;
extern       APV_TYPE_BOOLEAN                 apvRfm69HcwCoreActiveFlag;
extern       APV_TYPE_BOOLEAN                 apvRfm69HcwCoreSchedulingFlag;
extern       APV_TYPE_UINT8                   apvRfm69HcwManualResetCounter;
extern       APV_TYPE_UINT32                  apvRfm69HcwTimerIndex;

/******************************************************************************/

#endif

/******************************************************************************/
/* apv_RFM69HCW.h                                                             */
/******************************************************************************/
/* (C) PulsingCoreSoftware Limited 2020 (C)                                   */
/******************************************************************************/
